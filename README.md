# Opracowanie aplikacji w technologii mikrousług osadzonych na platformie Kubernetes
#### (Development of micro-service based application for the Kubernetes platform)

# Narzędzia wdrożenia

- [Repozytorium][gitlab]
- [Trello dashboard][trello]
- [OLD DATA] [Praca inżynierska][praca inzynierska] oraz [program pracy inżynierskiej][old settler]

# Plan wdrożenia
1. [refaktoryzacja](https://gitlab.com/littlem/master_thesis/tree/master/settler) istniejącejącej aplikacji w celu ułatwienia jej dalszej dekompozycji
2. wyodrębnienie mikrousług i określenie sposobu komunikacji pomiędzy nimi
	- usługa uwierzytelniająca użytkowników
		- baza danych użytkowników (np. MongoDB)
		- REST api (zarejestruj, zaloguj)

	- [dyspozytor zadań](https://gitlab.com/littlem/master_thesis/tree/master/settler/dispatcher)
		- asynchroniczne zapytania
		- REST api (bezpośrednio z frontendem)

	- [nadzorca (algorytmu)](https://gitlab.com/littlem/master_thesis/tree/master/settler/taskmaster)
		- przesyła wejściowe informacje i odbiera wyjściowe
		- pojedyncze zadanie = pojedynczy kontekst, który ginie po zakończeniu zadania

	- usługi operatorów genetycznych (selekcji, mutacji, krzyżowania) oraz generowania populacji
		- autoskalowalne (Kubernetes - ReplicaSet)

	- [komunikacja](https://gitlab.com/littlem/master_thesis/tree/master/settler/proto_files/proto) za pomocą frameworku [grpc] (binarnie - szybciej, łatwiej zachować protokół)

3. implementacja poszczególnych mikrousług wraz z testami jednostkowymi
4. sporządzenie wymaganych definicji obiektów w modelu Kubernetes dla każdej z mikrousług
5. implementacja systemu ciągłej integracji oraz ciągłego dostarczania dla każdej z mikrousług (Jenkins)
6. opracowanie automatycznych testów integracyjnych, bezpieczeństwa oraz wydajnościowych (Helm(?))

# Rozdziały

1. Wprowadzenie
    - cel pracy

2. Teoria
	1. Architektura mikrousług, a monolit
        - rys historyczny (webservice, SOA)
        - zalety mikrousług (kontekst biznesowy: np testerzy)
		- ryzyka związane z mikrousługami (infrastuktura, ilość mikrousług)

	2. Komunikacja między mikrousługami
		- bezpieczeństwo komunikacji - rys historyczny (autentykacja na przykładzie webtokena)
		- gRPC

	3. Konteneryzacja
		- wirtualizacja
		- konteneryzacja - Docker, LXC, Rkt
		- bezpieczeństwo

	4. Zarządzanie kontenerami aplikacyjnymi
		- porównanie rozwiązań pozwalających na zarządzanie kontenerami aplikacyjnymi: Kubernetes, Docker Compose, Docker Swarm, Mesos
		- rozliczenie zalet i wad

2. Aplikacja
	1. Prezentacja aplikacji
		- diagram przypadków użycia, diagram interakcji, diagram komponentów
		- reprezentacja aplikacji w platformie Kubernetes (schemat obiektów)
		- testy aplikacji

	2. Wdrożenie aplikacji
		- wdrożenie w chmurze publicznej
		- wdrożenie w chmurze publicznej oraz prywatnej (dane wrażliwe)
		- zarządzanie wdrożonymi aplikacjami

	4. Monitorowanie mikrousług
		- opis działania
		- przegląd dostępnych rozwiązań - dashboardów

4. Podsumowanie
	- zabranie wad i zalet rozwiązania



# Wstępna baza literatury

1. Architektura mikrousług, a monolit, komunikacja
	- "Microservices from Theory to Practice: Creating Applications in IBM Bluemix Using the Microservices Approach" - Chapter 1. Motivations for microservices -  Ramratan Vennam, Shishir Narain, Marcelo Martins, Valerie Lampkin, Sunil Joshi, Manav Gupta, Vasfi Gucer, Dejan Glozic, Carlos M Ferreira, Kameswara Eati, Nguyen Van Duy, Shahir Daya (zalety i wady mikrousług)

	- "Budowanie mikrousług" - Sam Newman

	- "Mikrousługi. Wdrażanie i standaryzacja systemów w organizacji inżynierskiej" - Susan J. Fowler (Helion - przekład polski)

	- "The State of Microservices Maturity" - Neal Ford

	- "Microservices for the Enterpsrise: Designing, Developing and Deploying" - Kasun Indrasiri, Prabath Siriwardena

	- "Hands-On Microservices – Monitoring and Testing" - Packt Publishing

	- "Enterprise Docker" - Christopher Tozzi


2. Bezpieczeństwo
	- "Preventing Security Threats to Microservices Architectures" - Sam Newman (Helion - przekład polski)

3. Zarządzanie kontenerami aplikacyjnymi
	- "Kubernetes: Up and Running" - Joe Beda, Brendan Burns, Kelsey Hightower
	- "Microservices and Containers, First edition" - Parminder Singh Kocher
	- "Deployment with Docker" - Srdjan Grubor

[grpc]: https://grpc.io/
[gitlab]: https://gitlab.com/littlem/master_thesis/tree/master
[trello]: https://trello.com/b/IoABqTH3
[old settler]: https://bitbucket.org/littlem7/settler/src/master/
[praca inzynierska]: https://bitbucket.org/littlem7/praca_inzynierska/src/master/
