#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock #pylint: disable=unused-import

import common.helpers
import select_operator

class TestSelectOperator(unittest.TestCase):
    server_class = select_operator.SelectOperator
    port = 1111
    test_message = "test_message_for_select_operator"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_SelectOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()
        ver1 = common.helpers.proto.settler_pb2.Vertex(label="A", x=0.5, y=0.5)
        ver2 = common.helpers.proto.settler_pb2.Vertex(label="B", x=0.0, y=0.0)
        center = common.helpers.proto.settler_pb2.Vertex(x=-1.0, y=-1.0)
        self.per1 = common.helpers.proto.settler_pb2.Person(name=ver1.label)
        self.per2 = common.helpers.proto.settler_pb2.Person(name=ver2.label)
        self.chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2])
        self.select_request = select_operator.SelectionRequest(
            map=common.helpers.proto.settler_pb2.UndirectedGraph(
                vertices=[ver1, ver2],
                edges=[common.helpers.proto.settler_pb2.Edge(
                    vertex1=ver1,
                    vertex2=ver2,
                    weight=4.0)]),
            priorities=common.helpers.proto.settler_pb2.UndirectedGraph(
                vertices=[ver1, ver2, center],
                edges=[
                    common.helpers.proto.settler_pb2.Edge(
                        vertex1=ver1,
                        vertex2=center,
                        weight=4.0),
                    common.helpers.proto.settler_pb2.Edge(
                        vertex1=ver2,
                        vertex2=center,
                        weight=1.0)]),
            relations=common.helpers.proto.settler_pb2.UndirectedGraph(
                vertices=[ver1, ver2],
                edges=[common.helpers.proto.settler_pb2.Edge(
                    vertex1=ver1,
                    vertex2=ver2,
                    weight=4.0)]),
            population=common.helpers.proto.settler_pb2.Population(
                chromosommes=[self.chromosomme]),
            central_point=center,
            relations_rate=0.5)

    def tearDown(self):
        self.server.stop(None)

    def test_produce_selected_population_for_empty(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
            empty_population = stub.ProduceSelectedPopulation(select_operator.SelectionRequest())
            self.assertEqual(len(empty_population.chromosommes), 0)

    def test_produce_selected_population_best(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
            population = stub.ProduceSelectedPopulation(self.select_request)
            self.assertEqual(population.best, self.chromosomme)

    def test_produce_selected_population_best_rate(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
            population = stub.ProduceSelectedPopulation(self.select_request)
            self.assertGreater(population.best_fit, 0)

    def test_produce_selected_population_len(self):
        population_length = 10
        for _ in range(population_length):
            self.select_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(order=[self.per2, self.per1])])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
            population = stub.ProduceSelectedPopulation(self.select_request)
            self.assertEqual(len(population.chromosommes), population_length + 1)

    @mock.patch('select_operator.random', return_value=0.0)
    def test_produce_selected_population_after_mocked_roulette(self, _):
        population_length = 10
        not_common_chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2])
        for _ in range(population_length):
            self.select_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(order=[self.per2, self.per1])])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
            population = stub.ProduceSelectedPopulation(self.select_request)
            for chromosomme in population.chromosommes:
                with self.subTest(chromosomme=chromosomme):
                    self.assertEqual(chromosomme, not_common_chromosomme)

    def test_produce_selected_population_logging(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.SelectOperatorStub(channel)
                _ = stub.ProduceSelectedPopulation(select_operator.SelectionRequest())
            self.assertEqual(len(log.records), 1)
