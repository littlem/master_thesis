# Select Operator
*(pol. Operator selekcji)*

## Responsibilities
- generating new population based on given population using Roulette Selection (from Genetic Algorithms theory)

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/select_operator
sudo docker build -t select_operator .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 select_operator # other ports could also be parsed here
```
