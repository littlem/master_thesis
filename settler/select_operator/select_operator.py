#!/usr/bin/env python
#pylint: disable=missing-docstring,too-many-arguments

import logging
import argparse
from random import random

from proto.settler_pb2_grpc import SelectOperatorServicer
from proto.settler_pb2 import Population, SelectionRequest #pylint: disable=unused-import
from common.helpers import start_common_server, get_edge_by_name, get_edge_by_id


logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('population_generator')

def get_adjustment_rel(rel_graph, gene_i, gene_j):
    return get_edge_by_name(rel_graph, gene_i, gene_j).weight

def get_adjustment_map(map_graph, i, j):
    vertex1 = map_graph.vertices[i]
    vertex2 = map_graph.vertices[j]
    return get_edge_by_id(map_graph, vertex1, vertex2).weight

def get_adjustment_prior(prior_graph, central_point, vertex):
    return get_edge_by_id(prior_graph, vertex, central_point).weight

def get_adjustment_function(chromosomme, rel_graph, map_graph, prior_graph, \
        central_point, rel_rate):
    ret = 0.0
    for i, per1 in enumerate(chromosomme.order):
        for j, per2 in enumerate(chromosomme.order):
            if i > j:
                ret += rel_rate * get_adjustment_rel(rel_graph, per1, per2) \
                    / get_adjustment_map(map_graph, i, j)
    for i, person in enumerate(chromosomme.order):
        ret += (1.0 - rel_rate) * person.priority / \
            get_adjustment_prior(prior_graph, central_point, map_graph.vertices[i])
    return ret

def find_index_in_roulette_wheel(random_shot, roulette_wheel):
    for i, _ in enumerate(roulette_wheel):
        if i == 0 and random_shot <= roulette_wheel[0]:
            return 0
        if roulette_wheel[i] >= random_shot > roulette_wheel[i - 1]:
            return i
    raise Exception('Roulette shot out of roulette wheel range!')

class SelectOperator(SelectOperatorServicer): #pylint: disable=too-few-public-methods

    def ProduceSelectedPopulation(self, request: SelectionRequest, context): #pylint: disable=too-many-locals
        LOGGER.debug(
            "New population based on selection request %s", request)
        # count adjustment for each chromosomme
        adjustments = [
            get_adjustment_function(
                chromosomme,
                request.relations,
                request.map,
                request.priorities,
                request.central_point,
                request.relations_rate)
            for chromosomme in request.population.chromosommes]
        # roulette wheel selection
        sum_of_adj = sum(adjustments)
        roulette_wheel = []
        for value in adjustments:
            field = value/sum_of_adj
            previous_field = 0
            if roulette_wheel:
                previous_field = roulette_wheel[len(roulette_wheel) - 1]
            roulette_wheel.append(field + previous_field)
        new_population = []
        for _ in range(len(request.population.chromosommes)):
            random_shot = random()
            new_population.append(
                request.population.chromosommes[
                    find_index_in_roulette_wheel(random_shot, roulette_wheel)])
        best = None
        best_fit = None
        if adjustments:
            max_index = adjustments.index(max(adjustments))
            best = request.population.chromosommes[max_index]
            best_fit = adjustments[max_index]

        return Population(
            chromosommes=new_population,
            best=best,
            best_fit=best_fit)

def get_args():
    parser = argparse.ArgumentParser(description='Select Operator for genetic algorithm.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for select_operator to run')
    return parser.parse_args()

def main():
    args = get_args()
    start_common_server('SelectOperator', SelectOperator, args.port)


if __name__ == '__main__':
    main()
