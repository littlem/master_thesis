#!/usr/bin/env python

from distutils.core import setup

setup(
    name='settler',
    version='0.1',
    description="Settler's python microservices",
    author='Anna Malgorzata Bogusz',
    author_email='ania_bogusz@wp.pl',
    url='https://gitlab.com/littlem/master_thesis/tree/master',
    packages=['proto', 'common'],
    install_requires=[])