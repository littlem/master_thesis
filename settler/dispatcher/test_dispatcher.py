#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock

import common.helpers
import dispatcher

class TestDispatcher(unittest.TestCase):
    server_class = dispatcher.Dispatcher
    port = 1110
    test_message = "test_message"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_DispatcherServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('dispatcher.DbMasterStub')
    @mock.patch('common.helpers.os.environ')
    def test_get_job_result(self, _, db_master_stub_mock):
        db_master_stub_mock().GetJobResult.return_value = \
            dispatcher.AlgorithmResult()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
            response = stub.GetJobResult(
                dispatcher.JobId(job_id=self.test_message))
        self.assertTrue(isinstance(response, dispatcher.AlgorithmResult))

    def test_get_job_result_without_environment(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
            with self.assertRaises(common.helpers.grpc.RpcError):
                _ = stub.GetJobResult(
                    dispatcher.JobId(job_id=self.test_message))

    @mock.patch('dispatcher.DbMasterStub')
    @mock.patch('common.helpers.os.environ')
    def test_get_job_result_logging(self, _, db_master_stub_mock):
        db_master_stub_mock().GetJobResult.return_value = \
            dispatcher.AlgorithmResult()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
                _ = stub.GetJobResult(dispatcher.JobId(
                    job_id=self.test_message))
            self.assertEqual(len(log.records), 4)

    @mock.patch('dispatcher.TaskmasterStub')
    @mock.patch('common.helpers.os.environ')
    @mock.patch('dispatcher.DbMasterStub')
    def test_handle_job_request(self, db_master_stub_mock, *_):
        db_master_stub_mock().RegisterJobId.return_value = \
            dispatcher.JobId(job_id=self.test_message)
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
            job_id_response = stub.HandleJobRequest(dispatcher.AlgorithmParams())
        self.assertTrue(isinstance(job_id_response, dispatcher.JobId))
        self.assertEqual(job_id_response.job_id, self.test_message)

    def test_handle_job_request_without_environment(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
            with self.assertRaises(common.helpers.grpc.RpcError):
                _ = stub.HandleJobRequest(dispatcher.AlgorithmParams())

    @mock.patch('dispatcher.TaskmasterStub')
    @mock.patch('common.helpers.os.environ')
    @mock.patch('dispatcher.DbMasterStub')
    def test_handle_job_request_logging(self, db_master_stub_mock, *_):
        db_master_stub_mock().RegisterJobId.return_value = \
            dispatcher.JobId(job_id=self.test_message)
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.DispatcherStub(channel)
                _ = stub.HandleJobRequest(dispatcher.AlgorithmParams())
            self.assertEqual(len(log.records), 8)
