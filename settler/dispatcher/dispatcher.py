#!/usr/bin/env python
#pylint: disable=missing-docstring

import logging
import argparse

from proto.settler_pb2 import AlgorithmParams, JobId, AlgorithmResult #pylint: disable=unused-import
from proto.settler_pb2_grpc import DispatcherServicer, DbMasterStub, TaskmasterStub
import common.helpers
from common.helpers import grpc_sync_call, grpc_async_call, verify_endpoint, start_common_server, \
    get_endpoint

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('dispatcher')


class Dispatcher(DispatcherServicer):

    def GetJobResult(self, request, context):
        LOGGER.debug("Received job request with job_id '%s'", request.job_id)
        LOGGER.debug("Ask db_master for results")
        algorithm_result_response = grpc_sync_call(
            request, *get_endpoint('DB_MASTER'), DbMasterStub, 'GetJobResult')
        return algorithm_result_response

    def HandleJobRequest(self, request, context):
        LOGGER.debug("Received job request with params '%s'", request)
        LOGGER.debug("Save params in db_master")
        job_id_response = grpc_sync_call(
            request, *get_endpoint('DB_MASTER'), DbMasterStub, 'RegisterJobId')
        LOGGER.debug("Registered job with id '%s'", job_id_response.job_id)
        LOGGER.debug("Triggering new taskmaster for job '%s'", job_id_response.job_id)
        with common.helpers.grpc.insecure_channel('{}:{}'.format(
                *get_endpoint('TASKMASTER'))) as channel:
            future = grpc_async_call(job_id_response, channel, TaskmasterStub, 'DoJobId')
            while not future.running():
                common.helpers.time.sleep(1)
        return job_id_response

def get_args():
    parser = argparse.ArgumentParser(description='Dispatcher for job triggering.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for dispatcher to run')
    return parser.parse_args()

def main():
    args = get_args()
    verify_endpoint('TASKMASTER')
    verify_endpoint('DB_MASTER')
    start_common_server('Dispatcher', Dispatcher, args.port)


if __name__ == '__main__':
    main()
