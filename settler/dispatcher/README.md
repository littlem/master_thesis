# Dispatcher
*(pol. Dyspozytor zadań)*

## Responsibilities
- async requests with gRPC from [ui_proxy] and to [taskmaster] and [db_master]
- gethering job's result from [db_master] and propagating this information to [ui_proxy]
- registering new job in [db_master]
- triggering new job via [taskmaster]

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/dispatcher
sudo docker build -t dispatcher .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 dispatcher # other ports could also be parsed here
```

[ui_proxy]: https://gitlab.com/littlem/master_thesis/tree/master/settler/ui_proxy
[taskmaster]: https://gitlab.com/littlem/master_thesis/tree/master/settler/taskmaster
[db_master]: https://gitlab.com/littlem/master_thesis/tree/master/settler/db_master