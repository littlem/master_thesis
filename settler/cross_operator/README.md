# Cross Operator
*(pol. Operator krzyzowania)*

## Responsibilities
- generating new population based on given population using Order Crossover (from Genetic Algorithms theory)

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/cross_operator
sudo docker build -t cross_operator .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 cross_operator # other ports could also be parsed here
```
