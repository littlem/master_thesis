#!/usr/bin/env python
#pylint: disable=missing-docstring,too-many-arguments

import logging
import argparse
from random import randint
from typing import List

from proto.settler_pb2_grpc import CrossOperatorServicer
from proto.settler_pb2 import Population, Chromosomme, GeneticOperatorRequest #pylint: disable=unused-import
from common.helpers import start_common_server, get_random_agents


logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('cross_operator')

def cross_chromosommes(agent1: Chromosomme, agent2: Chromosomme) -> List[Chromosomme]:
    chromosomme_lenght = len(agent1.order)
    matching_sec_loc = randint(1, chromosomme_lenght - 1)
    new_agent1 = []
    new_agent2 = []
    new_agent1[:] = agent1.order[:matching_sec_loc]
    new_agent2[:] = agent2.order[:matching_sec_loc]

    for gene in agent2.order[matching_sec_loc:] + agent2.order[:matching_sec_loc]:
        if gene not in new_agent1:
            new_agent1.append(gene)

    for gene in agent1.order[matching_sec_loc:] + agent1.order[:matching_sec_loc]:
        if gene not in new_agent2:
            new_agent2.append(gene)
    return [Chromosomme(order=new_agent1), Chromosomme(order=new_agent2)]


class CrossOperator(CrossOperatorServicer): #pylint: disable=too-few-public-methods

    def ProduceCrossedPopulation(self, request: GeneticOperatorRequest, context): #pylint: disable=too-many-locals
        LOGGER.debug("New population based on genetic operator request %s", request)
        population = list(request.population.chromosommes)
        # get random agents to cross and basic population
        agents_idx = get_random_agents(len(population), request.rate, parity=True)
        LOGGER.debug("These agents will be crossed %s", agents_idx)
        # order crossover perform and add to new population
        for i in range(int(len(agents_idx) / 2)):
            agent1, agent2 = cross_chromosommes(
                population[agents_idx[2 * i]],
                population[agents_idx[2 * i + 1]])
            population[agents_idx[2 * i]], population[agents_idx[2 * i + 1]] = agent1, agent2
        return Population(chromosommes=population)

def get_args():
    parser = argparse.ArgumentParser(description='Cross Operator for genetic algorithm.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for cross_operator to run')
    return parser.parse_args()

def main():
    args = get_args()
    start_common_server('CrossOperator', CrossOperator, args.port)


if __name__ == '__main__':
    main()
