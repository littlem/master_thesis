#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock

import common.helpers
import cross_operator

class TestCrossOperator(unittest.TestCase):
    server_class = cross_operator.CrossOperator
    port = 1111
    test_message = "test_message_for_cross_operator"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_CrossOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()
        ver1 = common.helpers.proto.settler_pb2.Vertex(label="A", x=0.5, y=0.5)
        ver2 = common.helpers.proto.settler_pb2.Vertex(label="B", x=0.0, y=0.0)
        ver3 = common.helpers.proto.settler_pb2.Vertex(label="C", x=0.0, y=0.0)
        self.per1 = common.helpers.proto.settler_pb2.Person(name=ver1.label)
        self.per2 = common.helpers.proto.settler_pb2.Person(name=ver2.label)
        self.per3 = common.helpers.proto.settler_pb2.Person(name=ver3.label)
        self.chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2, self.per3])
        self.cross_request = cross_operator.GeneticOperatorRequest(
            population=common.helpers.proto.settler_pb2.Population(
                chromosommes=[self.chromosomme]),
            rate=0.5)

    def tearDown(self):
        self.server.stop(None)

    def test_produce_crossed_population_for_empty(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.CrossOperatorStub(channel)
            empty_population = stub.ProduceCrossedPopulation(
                cross_operator.GeneticOperatorRequest())
            self.assertEqual(len(empty_population.chromosommes), 0)

    def test_produce_crossed_population_len(self):
        population_length = 10
        for _ in range(population_length):
            self.cross_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(
                    order=[self.per1, self.per2, self.per3])])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.CrossOperatorStub(channel)
            population = stub.ProduceCrossedPopulation(self.cross_request)
            self.assertEqual(len(population.chromosommes), population_length + 1)

    def test_produce_crossed_population_for_same_elements(self):
        population_length = 10
        for _ in range(population_length):
            self.cross_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(order=self.chromosomme.order)])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.CrossOperatorStub(channel)
            population = stub.ProduceCrossedPopulation(self.cross_request)
            for chromosomme in population.chromosommes:
                with self.subTest(chromosomme=chromosomme):
                    self.assertEqual(chromosomme.order, self.chromosomme.order)

    def test_produce_crossed_population_logging(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.CrossOperatorStub(channel)
                _ = stub.ProduceCrossedPopulation(cross_operator.GeneticOperatorRequest())
            self.assertEqual(len(log.records), 2)

    @mock.patch('cross_operator.randint', return_value=1)
    def test_cross_chromosommes(self, _):
        chromosomme1 = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2, self.per3])
        chromosomme2 = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per2, self.per3, self.per1])
        self.assertNotEqual(
            cross_operator.cross_chromosommes(chromosomme1, chromosomme2),
            (chromosomme1.order, chromosomme2.order))
