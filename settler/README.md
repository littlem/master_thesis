[![pipeline status](https://gitlab.com/littlem/master_thesis/badges/master/pipeline.svg)](https://gitlab.com/littlem/master_thesis/commits/master) [![coverage report](https://gitlab.com/littlem/master_thesis/badges/master/coverage.svg)](https://gitlab.com/littlem/master_thesis/commits/master)

# Settler
Resolving Seating Assignment Problem (SAP).

## Application's component diagram (settler architecture)

```plantuml
@startuml

left to right direction

' uslugi operatorow genetycznych
package "usługi operatorów genetycznych" {

component selekcja as select_operator
component mutacja as mutate_operator
component krzyżowanie as cross_operator
component "generacja populacji" as population_generator

interface populacja as p1
interface populacja as p2
interface populacja as p3
interface populacja as p4
interface "specyfikacja populacji" as p5

}

' inne usługi
component UI as ui
component [UI proxy] as ui_proxy
component dyspozytor as dispatcher
database {
    component [baza danych] as db_master
}
component nadzorca as taskmaster

interface REST
note right of REST : POST zadanie/GET rezultat

interface gRPC
note right of ui_proxy : translacja zapytania

' powiązania operatorów genetycznych
p1 -down-> select_operator
taskmaster <-- p1
mutate_operator <-up- p2
taskmaster <-- p2
cross_operator <-up- p3
taskmaster <-- p3
p5 -down-> population_generator 
population_generator -up- p4
taskmaster <-- p4
taskmaster -- p5

' powiazania dyspozytora zadan
db_master -down- taskmaster: "pobierz parametry zadania\nzapisz rezultat zadania"
dispatcher -down-> taskmaster : "rozpocznij zadanie"

ui -right- REST
REST -right- ui_proxy
ui_proxy -right- gRPC
gRPC -right- dispatcher
dispatcher -right- db_master : "zapisz id zadania\npobierz rezultat"

@enduml
```

## Application's sequence diagrams

Registering user's request.
```mermaid
sequenceDiagram

UI->>ui_proxy: POST JSON request
ui_proxy->>dispatcher: algorithm data
dispatcher->>db_master: algorithm data
db_master-->>dispatcher: job_id
dispatcher->>taskmaster: job_id
dispatcher-->>ui_proxy: job_id
ui_proxy-->>UI: job_id (200)
```

Gathering result of user's request.
```mermaid
sequenceDiagram

alt taskmaster doesn't finish his work yet
UI->>ui_proxy: GET job_id
ui_proxy->>dispatcher: job_id
dispatcher->>db_master: job_id
db_master-->>dispatcher: in progress
dispatcher-->>ui_proxy: in progress
ui_proxy-->>UI: in progress (202)
else taskmaster finished his work
UI->>ui_proxy: GET job_id
ui_proxy->>dispatcher: job_id
dispatcher->>db_master: job_id
db_master-->>dispatcher: best fit
dispatcher-->>ui_proxy: best fit
ui_proxy-->>UI: JSON - best fit (200)
end
```

Taskmaster's interactions.
```mermaid
sequenceDiagram

dispatcher->>taskmaster: job_id
taskmaster->>db_master: job_id
db_master-->>taskmaster: algorithm's parameters
taskmaster->>population_generator: chromosomme + population's quantity
population_generator-->>taskmaster: population
loop until max of iterations
taskmaster->>select_operator: relations, map, priorities graphs, population, <br/>central point, relations_rate, priorities
select_operator-->>taskmaster: population
taskmaster->>mutate_operator: population + mut_rate
mutate_operator-->>taskmaster: population
taskmaster->>cross_operator: population + cross_rate
cross_operator-->>taskmaster: population
end
taskmaster->>db_master: job_id + best fit
```

## Application's use case diagram

```plantuml
@startuml
skinparam packageStyle rectangle
left to right direction

actor :Użytkownik: as U1
actor :Taskmaster: as TM

rectangle settler {

    usecase UC2 as "
    Sprawdź rezultat zadania
    o zadanym id
    "

    usecase UC1 as "
    Zapytaj
    o najlepsze ułożenie 
    dla zadanej mapy, osób i relacji
    "
    
    usecase UC3 as "
    Stwórz zadanie
    i zwróć jego id
    "

    usecase UC4 as "
    Oblicz zadanie
    i zapisz jego rezultat
    "
}

U1 --> UC2
U1 --> UC1
UC2 .left.> UC4 : zależy od
UC1 .> UC3 : zawiera
UC3 .> UC4 : zawiera

UC3 <-- TM
UC4 <-- TM

@enduml
```

## Communication

grpc & [grpc-gateway][grpc-gateway] (gRPC to JSON proxy generator following the gRPC HTTP spec)

## Dev environment preparation

### Protobuf code generation

Clone repository
```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler
```

Create a virtualenv
```bash
virtualenv --python=python3.7 venv_master_thesis
source venv_master_thesis/bin/activate
```

Install dependencies
```bash
pip install -r requirements.txt
```

Generating the Python code for [gRPC][grpc python] and install it to be available for other microservices
```bash
python -m grpc_tools.protoc --python_out=. --grpc_python_out=. --proto_path=proto_files/ proto_files/proto/settler.proto
pip install .
```

### Swagger code (re)generation

[Generating][swagger generate] the Python (Flask) server from swagger configuration
```bash
sudo docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate \
    -i /local/ui_proxy/swagger_server/swagger/swagger.yaml \
    -l python-flask \
    -o /local/ui_proxy/
sudo chmod -R g+w ui_proxy
sudo chown -R ania ui_proxy/
```

### Tests

Unit tests triggering

```bash
pip install -r requirements-test.txt
pip install -r ui_proxy/test-requirements.txt
pytest
```

Pylint tests

```bash
pylint --ignore-patterns=settler_pb2 */*.py ui_proxy/swagger_server/*.py --disable=fixme,duplicate-code,no-member
```

### Preparing a base image for other python microservices
```bash
sudo docker build -t settler-python-config:latest .
```

### Image with tests (triggered by pipeline)
```bash
cd settler
sudo docker build -t settler-test -f test.Dockerfile .
sudo docker run settler-test
```

[grpc python]: https://grpc.io/docs/tutorials/basic/python/
[grpc-gateway]: https://grpc-ecosystem.github.io/grpc-gateway/
[grpc-gateway python]: https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/endpoints/bookstore-grpc-transcoding
[transcoding]: https://cloud.google.com/endpoints/docs/grpc/transcoding
[swagger generate]: https://github.com/swagger-api/swagger-codegen

# Kubernetes configuration

## Port forward locally
```bash
kubectl port-forward deployment/ui-proxy 8080:8080
```