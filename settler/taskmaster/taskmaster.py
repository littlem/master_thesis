#!/usr/bin/env python
#pylint: disable=missing-docstring,too-many-arguments

import logging
import argparse
import math
from random import shuffle
from typing import List

from proto.settler_pb2_grpc import TaskmasterServicer, DbMasterStub, PopulationGeneratorStub, \
    SelectOperatorStub, CrossOperatorStub, MutateOperatorStub
from proto.settler_pb2 import AlgorithmResult, Chromosomme, PopulationRequest, Population, \
    UndirectedGraph, Vertex, Edge, SelectionRequest, GeneticOperatorRequest, SettledPerson
from proto.settler_pb2 import Empty, JobId, AlgorithmParams, Person, Relation #pylint: disable=unused-import
import common.helpers
from common.helpers import grpc_sync_call, grpc_async_call, wait_for_all, verify_endpoint, \
    start_common_server, get_endpoint

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('taskmaster')

def get_best_for_populations(few_populations):
    best_chromosomme_adjustment = -math.inf
    best_chromosomme = None
    for population in few_populations:
        if best_chromosomme_adjustment < population.best_fit:
            best_chromosomme_adjustment = population.best_fit
            best_chromosomme = population.best
    return best_chromosomme, best_chromosomme_adjustment

def join_populations(few_populations):
    best, best_fit = get_best_for_populations(few_populations)
    gathered_population = Population(chromosommes=[], best=best, best_fit=best_fit)
    for population in few_populations:
        gathered_population.chromosommes.extend(population.chromosommes)
    return gathered_population

def split_call_population_generator(persons, quantity, split_rate):
    # FIXME: to join with split_call_select_operator
    small_quantity = int(quantity / split_rate) + 1
    LOGGER.debug(
        "Spawn (%d) generations with quantity %d for chromosmme %s",
        split_rate, small_quantity, persons)
    with common.helpers.grpc.insecure_channel('{}:{}'.format(
            *get_endpoint('POPULATION_GENERATOR'))) as channel:
        results = wait_for_all([
            grpc_async_call(
                PopulationRequest(
                    chromosomme=Chromosomme(order=persons),
                    quantity=small_quantity),
                channel,
                PopulationGeneratorStub,
                'ProducePopulation') for _ in range(split_rate)])
    return join_populations(results)

def split_population_randomly(population, parts_count):
    population_list = list(population.chromosommes)
    shuffle(population_list)
    small_quantity = int(len(population_list) / parts_count) + 1
    LOGGER.debug(
        "Spawn (%d) select operators with population's quantity %d",
        parts_count, small_quantity)
    populations = []
    i = 0
    for _ in range(parts_count):
        populations.append(population_list[i:i + small_quantity])
        i += small_quantity
    return populations

def split_call_select_operator(map_graph, relations_graph, priorities_graph, population, \
        central_point, relations_rate, split_rate):
    # FIXME: to join with split_call_population_generator
    small_quantity = int(len(population.chromosommes) / split_rate) + 1
    LOGGER.debug(
        "Spawn (%d) select operators with population's quantity %d",
        split_rate, small_quantity)
    splited_population = split_population_randomly(population, split_rate)
    with common.helpers.grpc.insecure_channel('{}:{}'.format(
            *get_endpoint('SELECT_OPERATOR'))) as channel:
        results = wait_for_all([
            grpc_async_call(
                SelectionRequest(
                    map=map_graph,
                    relations=relations_graph,
                    priorities=priorities_graph,
                    population=Population(chromosommes=splited_population[i]),
                    central_point=central_point,
                    relations_rate=relations_rate),
                channel,
                SelectOperatorStub,
                'ProduceSelectedPopulation') for i in range(split_rate)])
    return join_populations(results)

def split_call_genetic_operator(population, genetic_operator_rate, split_rate, \
        endpoint_name, request_class, stub_class, request_method):
    # FIXME: to join with split_call_population_generator
    small_quantity = int(len(population.chromosommes) / split_rate) + 1
    LOGGER.debug(
        "Spawn (%d) genetic operators with population's quantity %d",
        split_rate, small_quantity)
    splited_population = split_population_randomly(population, split_rate)
    with common.helpers.grpc.insecure_channel('{}:{}'.format(
            *get_endpoint(endpoint_name))) as channel:
        results = wait_for_all([
            grpc_async_call(
                request_class(
                    population=Population(chromosommes=splited_population[i]),
                    rate=genetic_operator_rate),
                channel,
                stub_class,
                request_method) for i in range(split_rate)])
    return join_populations(results)

def rescale_weights(dict_of_weights, max_weight=5, min_weight=-5):
    if dict_of_weights:
        minimum = min(dict_of_weights.values())
        maximum = max(dict_of_weights.values())
        if minimum == maximum:
            for key in dict_of_weights.keys():
                dict_of_weights[key] = max_weight
        else:
            for key in dict_of_weights.keys():
                dict_of_weights[key] = float((dict_of_weights[key] - minimum) \
                    * (max_weight - min_weight)) / (maximum - minimum) + min_weight

def create_relations_graph(persons, relations, default_weight: float):

    def get_weights(persons, relations, default_weight):
        weights = dict()
        for i, _ in enumerate(persons):
            for j in range(i + 1, len(persons)):
                weights[(persons[i].name, persons[j].name)] = []
        for relation in relations:
            if isinstance(weights.get((relation.fr, relation.to)), list):
                weights[(relation.fr, relation.to)].append(relation.weight)
            else:
                weights[(relation.to, relation.fr)].append(relation.weight)
        for key in weights:
            if not weights[key]:
                weights[key] = default_weight
            elif len(weights[key]) == 1:
                weights[key] = (weights[key][0] + default_weight) / 2.0
            else:
                weights[key] = sum(weights[key]) / len(weights[key])
        rescale_weights(weights)
        return weights

    LOGGER.debug("Creating graph for persons %s", persons)
    vertices = [Vertex(label=p.name, id=i+1) for i, p in enumerate(persons)]
    edges = []
    weights = get_weights(persons, relations, default_weight)
    for i, _ in enumerate(vertices):
        for j in range(i + 1, len(vertices)):
            if vertices[i].id != vertices[j].id:
                edges.append(Edge(
                    vertex1=vertices[i],
                    vertex2=vertices[j],
                    weight=weights[(vertices[i].label, vertices[j].label)]))
    return UndirectedGraph(vertices=vertices, edges=edges)

def count_distance(ver1: Vertex, ver2: Vertex) -> float:
    return math.hypot(ver2.x - ver1.x, ver2.y - ver1.y)

def create_map_graph(vertices):
    LOGGER.debug("Creating map graph for vertices %s", vertices)

    def get_map_weights(vertices, table_distance_weight=5):
        weights = dict()
        for i, _ in enumerate(vertices):
            for j in range(i + 1, len(vertices)):
                weights[(vertices[i].id, vertices[j].id)] = count_distance(vertices[i], vertices[j])
                if vertices[i].label != vertices[j].label:
                    weights[(vertices[i].id, vertices[j].id)] += table_distance_weight
        rescale_weights(weights, max_weight=5, min_weight=1)
        return weights

    edges = []
    weights = get_map_weights(vertices)
    for i, _ in enumerate(vertices):
        for j in range(i + 1, len(vertices)):
            if vertices[i].id != vertices[j].id:
                edges.append(Edge(
                    vertex1=vertices[i],
                    vertex2=vertices[j],
                    weight=weights[(vertices[i].id, vertices[j].id)]))
    return UndirectedGraph(vertices=vertices, edges=edges)

def create_priorities_graph(vertices, max_vertex):
    LOGGER.debug("Creating priorities graph for vertices %s and max %s", vertices, max_vertex)

    def get_priorities_map_weights(vertices, max_vertex):
        weights = {v.id: count_distance(v, max_vertex) for v in vertices}
        rescale_weights(weights, max_weight=5, min_weight=1)
        return weights

    weights = get_priorities_map_weights(vertices, max_vertex)
    edges = [
        Edge(
            vertex1=v,
            vertex2=max_vertex,
            weight=weights[v.id])
        for v in vertices]
    return UndirectedGraph(vertices=list(vertices) + [max_vertex], edges=edges)

def chromosomme2fenotype(best_chromosomme, map_graph) -> List[SettledPerson]:
    return [
        SettledPerson(person=best_chromosomme.order[idx], vertex=map_vertex)
        for idx, map_vertex
        in enumerate(map_graph.vertices)]


class Taskmaster(TaskmasterServicer): #pylint: disable=too-few-public-methods

    def DoJobId(self, request, context):
        LOGGER.debug("Received job request with job_id '%s'", request.job_id)
        algorithm_params = grpc_sync_call(
            request, *get_endpoint('DB_MASTER'), DbMasterStub, 'GiveParamsForTaskmaster')
        population = split_call_population_generator(
            algorithm_params.persons,
            algorithm_params.population_quantity,
            algorithm_params.split_rate)
        relations_graph = create_relations_graph(
            algorithm_params.persons,
            algorithm_params.relations,
            algorithm_params.default_weight)
        map_graph = create_map_graph(algorithm_params.map)
        priorities_graph = create_priorities_graph(
            algorithm_params.map,
            algorithm_params.max_priority)
        best_chromosomme = Chromosomme() # pylint: disable=unused-variable
        best_chromosomme_adjustment = -math.inf
        for iteration in range(algorithm_params.iteration_max):
            LOGGER.debug("Iteration %d", iteration)
            population = split_call_select_operator(map_graph, relations_graph, priorities_graph, \
                population, algorithm_params.max_priority, algorithm_params.relations_rate, \
                algorithm_params.split_rate)
            LOGGER.debug("Count best %f", population.best_fit)
            if best_chromosomme_adjustment < population.best_fit:
                best_chromosomme_adjustment = population.best_fit
                best_chromosomme = population.best
            population = split_call_genetic_operator(
                population, algorithm_params.cross_rate, algorithm_params.split_rate,
                'CROSS_OPERATOR', GeneticOperatorRequest, CrossOperatorStub,
                'ProduceCrossedPopulation')
            population = split_call_genetic_operator(
                population, algorithm_params.mutation_rate, algorithm_params.split_rate,
                'MUTATE_OPERATOR', GeneticOperatorRequest, MutateOperatorStub,
                'ProduceMutatedPopulation')
        algorithm_result = AlgorithmResult(
            status=AlgorithmResult.READY,
            result=chromosomme2fenotype(best_chromosomme, map_graph),
            result_fit=best_chromosomme_adjustment)
        return grpc_sync_call(
            algorithm_result, *get_endpoint('DB_MASTER'), DbMasterStub, 'SaveResultToDb')

def get_args():
    parser = argparse.ArgumentParser(description='Taskmaster for SAP resolving.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for taskmaster to run')
    return parser.parse_args()

def main():
    args = get_args()
    verify_endpoint('CROSS_OPERATOR')
    verify_endpoint('MUTATE_OPERATOR')
    verify_endpoint('SELECT_OPERATOR')
    verify_endpoint('DB_MASTER')
    verify_endpoint('POPULATION_GENERATOR')
    start_common_server('Taskmaster', Taskmaster, args.port)


if __name__ == '__main__':
    main()
