# Taskmaster

*(pol. Nadzorca algorytmu)*

## Responsibilities
- gather in/out requests
- executes job in a single context which will die after execution

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/taskmaster
sudo docker build -t taskmaster .
sudo docker run -it -p 8080:8080 taskmaster
```