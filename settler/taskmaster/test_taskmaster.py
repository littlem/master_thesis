#!/usr/bin/env python
#pylint: disable=missing-docstring,invalid-name,too-many-public-methods

import unittest
from unittest import mock

import common.helpers
import taskmaster

class TestTaskmaster(unittest.TestCase):
    server_class = taskmaster.Taskmaster
    port = 1111
    test_message = "test_message_for_taskmaster"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_TaskmasterServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('taskmaster.get_best_for_populations', return_value=(None, 0.0))
    @mock.patch('taskmaster.PopulationGeneratorStub')
    @mock.patch('common.helpers.os.environ')
    @mock.patch('taskmaster.DbMasterStub')
    def test_do_job_id(self, db_master_stub_mock, *_):
        db_master_stub_mock().GiveParamsForTaskmaster.return_value = \
            taskmaster.AlgorithmParams(split_rate=1)
        db_master_stub_mock().SaveResultToDb.return_value = \
            taskmaster.Empty()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.TaskmasterStub(channel)
            response = stub.DoJobId(
                taskmaster.JobId(job_id=self.test_message))
        self.assertTrue(isinstance(response, taskmaster.Empty))

    def test_do_job_id_without_environment(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.TaskmasterStub(channel)
            with self.assertRaises(common.helpers.grpc.RpcError):
                _ = stub.DoJobId(
                    taskmaster.JobId(job_id=self.test_message))

    @mock.patch('taskmaster.get_best_for_populations', return_value=(None, 0.0))
    @mock.patch('taskmaster.PopulationGeneratorStub')
    @mock.patch('common.helpers.os.environ')
    @mock.patch('taskmaster.DbMasterStub')
    def test_do_job_id_logging(self, db_master_stub_mock, *_):
        db_master_stub_mock().GiveParamsForTaskmaster.return_value = \
            taskmaster.AlgorithmParams(split_rate=1)
        db_master_stub_mock().SaveResultToDb.return_value = \
            taskmaster.Empty()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.TaskmasterStub(channel)
                _ = stub.DoJobId(taskmaster.JobId(
                    job_id=self.test_message))
            self.assertEqual(len(log.records), 11)

    @mock.patch('taskmaster.get_best_for_populations', return_value=(None, 0.0))
    def test_join_populations(self, _):
        population_1 = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[
                taskmaster.Person(name='p1'),
                taskmaster.Person(name='p2')
            ]),
            taskmaster.Chromosomme(order=[
                taskmaster.Person(name='p3'),
                taskmaster.Person(name='p4')
            ])
        ])
        population_2 = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[
                taskmaster.Person(name='p5'),
                taskmaster.Person(name='p6')
            ])
        ])
        self.assertEqual(
            len(taskmaster.join_populations([population_1, population_2]).chromosommes), 3)

    def test_get_best_for_population(self):
        best_fit = 3.0
        normal_fit = 1.0
        best = taskmaster.Chromosomme()
        normal = None
        populations = [
            taskmaster.Population(best_fit=normal_fit, best=normal),
            taskmaster.Population(best_fit=best_fit, best=best),
            taskmaster.Population(best_fit=normal_fit, best=normal)]
        self.assertEqual(
            taskmaster.get_best_for_populations(populations),
            (best, best_fit))

    def test_get_best_for_population_empty_fit(self):
        populations = [
            taskmaster.Population(),
            taskmaster.Population()]
        self.assertEqual(
            taskmaster.get_best_for_populations(populations),
            (taskmaster.Chromosomme(), 0.0))

    @mock.patch('taskmaster.get_best_for_populations', return_value=(None, 1.0))
    def test_join_populations_with_best(self, method_mock):
        mocked_best = (taskmaster.Chromosomme(), 3.0)
        method_mock.return_value = mocked_best
        new_population = taskmaster.join_populations([])
        self.assertEqual(
            (new_population.best, new_population.best_fit),
            mocked_best)

    def test_split_population_randomly_for_empty_population_len(self):
        parts = 12
        populations = taskmaster.split_population_randomly(
            taskmaster.Population(chromosommes=[]),
            parts)
        self.assertEqual(len(populations), parts)

    def test_split_population_randomly_for_empty_population_content(self):
        parts = 12
        populations = taskmaster.split_population_randomly(
            taskmaster.Population(chromosommes=[]),
            parts)
        for population in populations:
            with self.subTest(population=population):
                self.assertEqual(population, [])

    def test_split_population_randomly_for_non_empty_population(self):
        parts = 10
        population_lenght = 7
        population = taskmaster.Population(chromosommes=[])
        for _ in range(population_lenght):
            population.chromosommes.extend([taskmaster.Chromosomme(order=[
                taskmaster.Person(name='p1'),
                taskmaster.Person(name='p2')
            ])])
        populations = taskmaster.split_population_randomly(
            population,
            parts)
        self.assertEqual(sum([len(p) for p in populations]), population_lenght)

    def test_rescale_weights(self):
        list_of_weights = {'a': 0.0, 'b': 3.0, 'c': 10.0}
        min_max = (1, 5)
        taskmaster.rescale_weights(list_of_weights, *min_max)
        self.assertEqual(
            (min(list_of_weights.values()), max(list_of_weights.values())),
            min_max)

    def test_rescale_weights_same_min_max(self):
        list_of_weights = {'a': 0.0, 'b': 3.0, 'c': 10.0}
        min_max = (1, 1)
        taskmaster.rescale_weights(list_of_weights, *min_max)
        for k, v in list_of_weights.items():
            with self.subTest(key=k):
                self.assertEqual(v, min_max[0])

    def test_count_distance(self):
        self.assertEqual(
            taskmaster.count_distance(
                taskmaster.Vertex(x=0.0, y=0.0),
                taskmaster.Vertex(x=0.0, y=2.0)),
            2.0)

    def test_chromosomme2fenotype(self):
        per1 = taskmaster.Person(name='p1')
        per2 = taskmaster.Person(name='p2')
        per3 = taskmaster.Person(name='p3')
        per4 = taskmaster.Person(name='p4')
        ver1 = taskmaster.Vertex()
        ver2 = taskmaster.Vertex()
        ver3 = taskmaster.Vertex()
        ver4 = taskmaster.Vertex()
        fenotype = [
            taskmaster.SettledPerson(person=per4, vertex=ver1),
            taskmaster.SettledPerson(person=per3, vertex=ver2),
            taskmaster.SettledPerson(person=per2, vertex=ver3),
            taskmaster.SettledPerson(person=per1, vertex=ver4)]
        chromosomme = taskmaster.Chromosomme(order=[per4, per3, per2, per1])
        self.assertEqual(
            taskmaster.chromosomme2fenotype(
                chromosomme, taskmaster.UndirectedGraph(vertices=[ver1, ver2, ver3, ver4])),
            fenotype)

    def test_chromosomme2fenotype_for_epty_chromosomme(self):
        self.assertEqual(
            taskmaster.chromosomme2fenotype(
                taskmaster.Chromosomme(order=[]),
                taskmaster.UndirectedGraph(vertices=[])),
            [])


class TestTaskmasterWithPopulationGeneratorMicroservice(unittest.TestCase):
    class TmpPopulationGenerator(common.helpers.proto.settler_pb2_grpc.PopulationGeneratorServicer): #pylint: disable=too-few-public-methods
        mocked_quantity = 10
        mocked_answer = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[taskmaster.Person(name='aaa')])
            for _ in range(10)])
        def ProducePopulation(self, request, context):
            return self.mocked_answer
    server_class = TmpPopulationGenerator
    port = 1111

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_PopulationGeneratorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_population_generator(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        split_rate = 1
        new_population = taskmaster.split_call_population_generator(
            self.server_class.mocked_answer.chromosommes[0].order,
            self.server_class.mocked_quantity,
            split_rate)
        self.assertEqual(new_population.chromosommes, self.server_class.mocked_answer.chromosommes)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_population_generator_for_few_calls(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        split_rate = 10
        new_population = taskmaster.split_call_population_generator(
            self.server_class.mocked_answer.chromosommes[0].order,
            self.server_class.mocked_quantity,
            split_rate)
        self.assertEqual(
            len(new_population.chromosommes),
            self.server_class.mocked_quantity * split_rate)


class TestTaskmasterWithSelectOperatorMicroservice(unittest.TestCase):
    class TmpSelectOperator(common.helpers.proto.settler_pb2_grpc.SelectOperatorServicer): #pylint: disable=too-few-public-methods
        mocked_quantity = 10
        mocked_answer = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[taskmaster.Person(name='aaa')])
            for _ in range(10)])
        def ProduceSelectedPopulation(self, request, context):
            return self.mocked_answer
    server_class = TmpSelectOperator
    port = 1111

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_SelectOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_select_operator(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        split_rate = 1
        new_population = taskmaster.split_call_select_operator(
            taskmaster.UndirectedGraph(),
            taskmaster.UndirectedGraph(),
            taskmaster.UndirectedGraph(),
            taskmaster.Population(),
            taskmaster.Vertex(),
            0.3,
            split_rate)
        self.assertEqual(new_population.chromosommes, self.server_class.mocked_answer.chromosommes)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_select_operator_for_few_calls(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        split_rate = 10
        new_population = taskmaster.split_call_select_operator(
            taskmaster.UndirectedGraph(),
            taskmaster.UndirectedGraph(),
            taskmaster.UndirectedGraph(),
            taskmaster.Population(),
            taskmaster.Vertex(),
            0.3,
            split_rate)
        self.assertEqual(
            len(new_population.chromosommes),
            self.server_class.mocked_quantity * split_rate)


class TestTaskmasterWithCrossOperatorMicroservice(unittest.TestCase):
    class TmpCrossOperator(common.helpers.proto.settler_pb2_grpc.CrossOperatorServicer): #pylint: disable=too-few-public-methods
        mocked_quantity = 10
        mocked_answer = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[taskmaster.Person(name='aaa')])
            for _ in range(10)])
        def ProduceCrossedPopulation(self, request, context):
            return self.mocked_answer
    server_class = TmpCrossOperator
    port = 1111

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_CrossOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_genetic_operator_for_crossing(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        population = taskmaster.Population(chromosommes=[])
        cross_rate = 0.3
        split_rate = 1
        new_population = taskmaster.split_call_genetic_operator(
            population,
            cross_rate,
            split_rate,
            'CROSS_OPERATOR',
            taskmaster.GeneticOperatorRequest,
            taskmaster.CrossOperatorStub,
            'ProduceCrossedPopulation')
        self.assertEqual(new_population.chromosommes, self.server_class.mocked_answer.chromosommes)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_genetic_operator_for_crossing_for_few_calls(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        population = taskmaster.Population(chromosommes=[])
        cross_rate = 0.3
        split_rate = 10
        new_population = taskmaster.split_call_genetic_operator(
            population,
            cross_rate,
            split_rate,
            'CROSS_OPERATOR',
            taskmaster.GeneticOperatorRequest,
            taskmaster.CrossOperatorStub,
            'ProduceCrossedPopulation')
        self.assertEqual(
            len(new_population.chromosommes),
            self.server_class.mocked_quantity * split_rate)


class TestTaskmasterWithMutateOperatorMicroservice(unittest.TestCase):
    class TmpMutateOperator(common.helpers.proto.settler_pb2_grpc.MutateOperatorServicer): #pylint: disable=too-few-public-methods
        mocked_quantity = 10
        mocked_answer = taskmaster.Population(chromosommes=[
            taskmaster.Chromosomme(order=[taskmaster.Person(name='aaa')])
            for _ in range(10)])
        def ProduceMutatedPopulation(self, request, context):
            return self.mocked_answer
    server_class = TmpMutateOperator
    port = 1111

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_MutateOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_genetic_operator_for_mutation(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        population = taskmaster.Population(chromosommes=[])
        mut_rate = 0.3
        split_rate = 1
        new_population = taskmaster.split_call_genetic_operator(
            population,
            mut_rate,
            split_rate,
            'MUTATE_OPERATOR',
            taskmaster.GeneticOperatorRequest,
            taskmaster.MutateOperatorStub,
            'ProduceMutatedPopulation')
        self.assertEqual(new_population.chromosommes, self.server_class.mocked_answer.chromosommes)

    @mock.patch('taskmaster.get_endpoint')
    def test_split_call_genetic_operator_for_mutation_for_few_calls(self, get_endpoint_mock):
        get_endpoint_mock.return_value = ('localhost', self.port)
        population = taskmaster.Population(chromosommes=[])
        mut_rate = 0.3
        split_rate = 10
        new_population = taskmaster.split_call_genetic_operator(
            population,
            mut_rate,
            split_rate,
            'MUTATE_OPERATOR',
            taskmaster.GeneticOperatorRequest,
            taskmaster.MutateOperatorStub,
            'ProduceMutatedPopulation')
        self.assertEqual(
            len(new_population.chromosommes),
            self.server_class.mocked_quantity * split_rate)


class TestTaskmasterCreateRelationsGraph(unittest.TestCase):

    def setUp(self):
        self.persons = [taskmaster.Person(name=name) for name in ["A", "B", "C", "D", "E"]]
        self.relations = [
            taskmaster.Relation(fr="A", to="B", weight=5.0),
            taskmaster.Relation(fr="B", to="C", weight=-7.0)]
        self.default_weight = 0.0
        self.graph = taskmaster.create_relations_graph(
            self.persons,
            self.relations,
            self.default_weight)
        self.max_weight = 5.0

    def test_graph_len(self):
        self.assertEqual(
            len(self.graph.vertices),
            len(self.persons))

    def test_all_ids(self):
        for v in self.graph.vertices:
            with self.subTest(vertex=v):
                self.assertNotEqual(v.id, 0)

    def test_complete_graph(self):
        n = len(self.persons)
        self.assertEqual(len(self.graph.edges), int(n * (n-1) / 2))

    def test_similar_weighted_edges(self):
        e1 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="C"), taskmaster.Person(name="D"))
        e2 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="D"), taskmaster.Person(name="E"))
        self.assertEqual(e1.weight, e2.weight)

    def test_weighted_edge_greater_then_0(self):
        e1 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="A"), taskmaster.Person(name="B"))
        self.assertGreater(e1.weight, 0)

    def test_weighted_edge_less_then_0(self):
        e1 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="B"), taskmaster.Person(name="C"))
        self.assertLess(e1.weight, 0)

    def test_scaled_greater_then_0(self):
        e1 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="A"), taskmaster.Person(name="B"))
        self.assertEqual(e1.weight, self.max_weight)

    def test_scaled_less_then_0(self):
        e1 = taskmaster.common.helpers.get_edge_by_name(
            self.graph, taskmaster.Person(name="B"), taskmaster.Person(name="C"))
        self.assertEqual(e1.weight, -self.max_weight)

class TestTaskmasterCreateMapGraph(unittest.TestCase): #pylint: disable=too-many-instance-attributes

    def setUp(self):
        self.v1 = taskmaster.Vertex(id=1, x=0, y=0, label="s1")
        self.v2 = taskmaster.Vertex(id=2, x=0, y=1, label="s1")
        self.v3 = taskmaster.Vertex(id=3, x=-1, y=1, label="s1")
        self.v4 = taskmaster.Vertex(id=4, x=-1, y=0, label="s2")
        self.vertices = [self.v1, self.v2, self.v3, self.v4]
        self.graph = taskmaster.create_map_graph(self.vertices)
        self.min_weight = 1
        self.max_weight = 5

    def test_graph_len(self):
        self.assertEqual(
            len(self.graph.vertices),
            len(self.vertices))

    def test_complete_graph(self):
        n = len(self.vertices)
        self.assertEqual(len(self.graph.edges), int(n * (n-1) / 2))

    def test_similar_weighted_edges(self):
        e1 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v1, self.v2)
        e2 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v3, self.v2)
        self.assertEqual(e1.weight, e2.weight)

    def test_scaled_edge_greater_then_minimum(self):
        for e in self.graph.edges:
            with self.subTest(edge=e):
                self.assertGreaterEqual(e.weight, self.min_weight)

    def test_scaled_edge_less_then_maximum(self):
        for e in self.graph.edges:
            with self.subTest(edge=e):
                self.assertLessEqual(e.weight, self.max_weight)

    def test_weighted_edge_different_table(self):
        e1 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v3, self.v2)
        e2 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v3, self.v4)
        self.assertGreater(e2.weight, e1.weight)

class TestTaskmasterCreatePrioritiesGraph(unittest.TestCase): #pylint: disable=too-many-instance-attributes

    def setUp(self):
        self.v1 = taskmaster.Vertex(id=1, x=0, y=10, label="s1")
        self.v2 = taskmaster.Vertex(id=2, x=-10, y=0, label="s2")
        self.max_v = taskmaster.Vertex(id=5, x=0, y=0, label="s2")
        self.vertices = [self.v1, self.v2]
        self.graph = taskmaster.create_priorities_graph(self.vertices, self.max_v)
        self.min_weight = 1
        self.max_weight = 5

    def test_graph_len(self):
        self.assertEqual(
            len(self.graph.vertices),
            len(self.vertices) + 1)

    def test_incomplete_graph(self):
        n = len(self.vertices)
        self.assertEqual(len(self.graph.edges), n)

    def test_similar_weighted_edges(self):
        e1 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v1, self.max_v)
        e2 = taskmaster.common.helpers.get_edge_by_id(self.graph, self.v2, self.max_v)
        self.assertEqual(e1.weight, e2.weight)

    def test_scaled_edge_greater_then_minimum(self):
        for e in self.graph.edges:
            with self.subTest(edge=e):
                self.assertGreaterEqual(e.weight, self.min_weight)

    def test_scaled_edge_less_then_maximum(self):
        for e in self.graph.edges:
            with self.subTest(edge=e):
                self.assertLessEqual(e.weight, self.max_weight)
