#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock

import common.helpers
import db_master

class TestDbMaster(unittest.TestCase):
    server_class = db_master.DbMaster
    port = 1111
    test_message = "test_message_for_db_master"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_DbMasterServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_get_job_result_for_non_existant(self, _, redis_mock):
        redis_mock().get.return_value = None
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            non_existant_result = stub.GetJobResult(
                db_master.JobId(job_id=self.test_message))
            self.assertEqual(non_existant_result.status, db_master.AlgorithmResult.NON_EXISTANT)

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_get_job_result_for_existant(self, _, redis_mock):
        mocked_result = db_master.AlgorithmResult(status=db_master.AlgorithmResult.IN_PROGRESS)
        redis_mock().get.return_value = mocked_result.SerializeToString()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            existant_result = stub.GetJobResult(
                db_master.JobId(job_id=self.test_message))
            self.assertEqual(existant_result, mocked_result)

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_register_job_id_with_first_call(self, _, redis_mock):
        mocked_split_rate = 16
        mocked_request = db_master.AlgorithmParams(split_rate=mocked_split_rate)
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            job_id = stub.RegisterJobId(
                mocked_request)
        redis_mock().set.assert_any_call(job_id.job_id, mocked_request.SerializeToString())

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_register_job_id_with_second_call(self, _, redis_mock):
        mocked_split_rate = 16
        mocked_request = db_master.AlgorithmParams(split_rate=mocked_split_rate)
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            job_id = stub.RegisterJobId(
                mocked_request)
        real_result = db_master.AlgorithmResult()
        real_result.ParseFromString(redis_mock().set.call_args[0][1])
        self.assertEqual(
            (real_result.status, real_result.job_id),
            (db_master.AlgorithmResult.CREATED, job_id.job_id))

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_save_result_to_db_for_non_existant_job_id(self, _, redis_mock):
        redis_mock().get.return_value = None
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            _ = stub.SaveResultToDb(
                db_master.AlgorithmResult())
        redis_mock().set.assert_not_called()

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_save_result_to_db_for_existant_job_id(self, _, redis_mock):
        mocked_result = db_master.AlgorithmResult(
            job_id=self.test_message,
            status=db_master.AlgorithmResult.IN_PROGRESS)
        redis_mock().get.return_value = mocked_result.SerializeToString()
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            _ = stub.SaveResultToDb(
                mocked_result)
        redis_mock().set.assert_called_once_with(
            self.test_message,
            db_master.AlgorithmResult(
                job_id=self.test_message,
                status=db_master.AlgorithmResult.READY).SerializeToString())


    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_give_params_for_taskmaster_non_existant_job_id(self, _, redis_mock):
        redis_mock().get.return_value = None
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            _ = stub.GiveParamsForTaskmaster(
                db_master.JobId())
        redis_mock().set.assert_not_called()

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_give_params_for_taskmaster_existant_job_id_with_result(self, _, redis_mock):
        mocked_result = db_master.AlgorithmResult(
            job_id=self.test_message,
            status=db_master.AlgorithmResult.CREATED)
        mocked_params = db_master.AlgorithmParams(split_rate=4)
        redis_mock().get.side_effect = [
            mocked_result.SerializeToString(),
            mocked_params.SerializeToString()]
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            _ = stub.GiveParamsForTaskmaster(
                db_master.JobId(job_id=self.test_message))
        redis_mock().set.assert_called_once_with(
            self.test_message,
            db_master.AlgorithmResult(
                job_id=self.test_message,
                status=db_master.AlgorithmResult.IN_PROGRESS).SerializeToString())

    @mock.patch('redis.Redis')
    @mock.patch('common.helpers.os.environ')
    def test_give_params_for_taskmaster_existant_job_id_with_params(self, _, redis_mock):
        mocked_result = db_master.AlgorithmResult(
            job_id=self.test_message,
            status=db_master.AlgorithmResult.CREATED)
        mocked_params = db_master.AlgorithmParams(split_rate=4)
        redis_mock().get.side_effect = [
            mocked_result.SerializeToString(),
            mocked_params.SerializeToString()]
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.DbMasterStub(channel)
            params = stub.GiveParamsForTaskmaster(
                db_master.JobId(job_id=self.test_message))
        self.assertEqual(params, mocked_params)
