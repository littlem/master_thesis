#!/usr/bin/env python
#pylint: disable=missing-docstring

import logging
import argparse

from enum import Enum

import redis

from proto.settler_pb2_grpc import DbMasterServicer
from proto.settler_pb2 import AlgorithmResult
from proto.settler_pb2 import Empty, JobId, AlgorithmParams
from common.helpers import verify_endpoint, start_common_server, get_endpoint

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('db_master')

class InfoType(Enum):
    PARAMS = 0
    STATUS = 1
    RESULT = 2

def open_redis_db(info_type: InfoType):
    redis_host, redis_port = get_endpoint('REDIS_DB')
    return redis.Redis(host=redis_host, port=redis_port, db=info_type.value)

# AlgorithmResult.ParseFromString(a.SerializeToString())

class DbMaster(DbMasterServicer):

    def GiveParamsForTaskmaster(self, request, context):
        LOGGER.debug("Received job request with job_id '%s'", request.job_id)
        params = AlgorithmParams()
        job_result = AlgorithmResult()
        status_db = open_redis_db(InfoType.STATUS)
        current_status = status_db.get(request.job_id)
        if not current_status:
            LOGGER.error("There's no job with id '%s'", request.job_id)
            return params
        job_result.ParseFromString(current_status)
        if job_result.status == AlgorithmResult.READY:
            LOGGER.error("Job is ready already!")
            return params
        if job_result.status == AlgorithmResult.IN_PROGRESS:
            LOGGER.warning("Job is in progress already!")
        raw_params = open_redis_db(InfoType.PARAMS).get(request.job_id)
        params.ParseFromString(raw_params)
        LOGGER.debug("Received params '%s'", params)
        # switch status for job_id
        job_result.status = AlgorithmResult.IN_PROGRESS
        status_db.set(request.job_id, job_result.SerializeToString())
        return params

    def SaveResultToDb(self, request, context):
        LOGGER.debug("Received algorithm result request with job_id '%s'", request.job_id)
        job_result = AlgorithmResult()
        status_db = open_redis_db(InfoType.STATUS)
        current_status = status_db.get(request.job_id)
        if not current_status:
            LOGGER.error("There's no job with id '%s'", request.job_id)
            return Empty()
        job_result.ParseFromString(current_status)
        if job_result.status == AlgorithmResult.READY:
            LOGGER.error("Job is ready already!")
            return Empty()
        if job_result.status == AlgorithmResult.NON_EXISTANT:
            LOGGER.error("Job won't triggered yet!")
            return Empty()
        request.status = AlgorithmResult.READY
        status_db.set(request.job_id, request.SerializeToString())
        return Empty()

    def RegisterJobId(self, request, context):
        LOGGER.debug("Received algorithm params '%s'", request)
        new_job_id = str(id(request))
        LOGGER.debug("Saving with job_id '%s'", new_job_id)
        open_redis_db(InfoType.PARAMS).set(new_job_id, request.SerializeToString())
        job_result = AlgorithmResult(status=AlgorithmResult.CREATED, job_id=new_job_id)
        open_redis_db(InfoType.STATUS).set(new_job_id, job_result.SerializeToString())
        return JobId(job_id=new_job_id)

    def GetJobResult(self, request, context):
        LOGGER.debug("Received job request with job_id '%s'", request.job_id)
        job_result = AlgorithmResult()
        status_db = open_redis_db(InfoType.STATUS)
        current_status = status_db.get(request.job_id)
        if not current_status:
            LOGGER.warning("There's no job with id '%s'", request.job_id)
            return AlgorithmResult(status=AlgorithmResult.NON_EXISTANT)
        job_result.ParseFromString(current_status)
        LOGGER.debug("Fetched result '%s'", job_result)
        return job_result

def get_args():
    parser = argparse.ArgumentParser(description='DbMaster for SAP resolving.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for db_master to run')
    return parser.parse_args()

def main():
    args = get_args()
    verify_endpoint('REDIS_DB')
    start_common_server('DbMaster', DbMaster, args.port)


if __name__ == '__main__':
    main()
