# DB master
*(pol. Operator bazy danych)*

## Responsibilities
- saving to DB and fetching from DB task parameters and result
- using simple DB PostgreSQL (?)

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/db_master
sudo docker build -t db_master .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 db_master # other ports could also be parsed here
```

## Building test redis db
```bash
docker run --name some-redis -d -p 6379:6379 redis
```
