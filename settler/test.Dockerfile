FROM settler-python-config:latest

ENV PYLINT_PARAMS "--disable=fixme,duplicate-code,no-member,import-outside-toplevel"

ADD requirements-test.txt /settler/
RUN pip install -r /settler/requirements-test.txt

ADD ui_proxy/test-requirements.txt /settler/ui_proxy/
RUN pip install -r /settler/ui_proxy/test-requirements.txt

ADD . /settler/
CMD python -m pytest --cov-config=/settler/.coveragerc --cov=/settler/ /settler/ -v && pylint --ignore-patterns=settler_pb2 /settler/ui_proxy/swagger_server/*.py /settler/*/*.py $PYLINT_PARAMS
