#!/usr/bin/env python
#pylint: disable=missing-docstring,invalid-name

import time
import unittest
from unittest import mock

import helpers

class TestHelpers(unittest.TestCase):

    def test_verify_endpoint_proper_env(self):
        service_name = 'TMP'
        environ_mock = mock.patch.dict(
            'helpers.os.environ',
            {
                f'{service_name}_HOSTNAME': 'tmp1',
                f'{service_name}_PORT': 'tmp2'
            })
        environ_mock.start()
        self.assertFalse(helpers.verify_endpoint(service_name))
        environ_mock.stop()

    @mock.patch.dict('helpers.os.environ', {})
    def test_verify_endpoint_faulty_env(self):
        service_name = 'TMP'
        with self.assertRaisesRegex(helpers.MissingEnvironmentVariableException, service_name):
            helpers.verify_endpoint(service_name)

    def test_get_endpoint_proper_env(self):
        service_name = 'TMP'
        hostname = 'tmp1'
        port = 'tmp2'
        environ_mock = mock.patch.dict(
            'helpers.os.environ',
            {
                f'{service_name}_HOSTNAME': hostname,
                f'{service_name}_PORT': port
            })
        environ_mock.start()
        self.assertEqual(
            (hostname, port),
            helpers.get_endpoint(service_name))
        environ_mock.stop()

    @mock.patch.dict('helpers.os.environ', {})
    def test_get_endpoint_faulty_env(self):
        service_name = 'TMP'
        with self.assertRaisesRegex(KeyError, service_name):
            helpers.get_endpoint(service_name)

    def test_start_common_server_for_service_outside_proto(self):
        service_name = 'this_is_my_service_name'
        service_class = object
        service_port = 4444
        with self.assertRaisesRegex(AttributeError, service_name):
            helpers.start_common_server(service_name, service_class, service_port)

    def test_start_common_server_for_service_from_proto_and_wrong_class(self):
        service_name = 'Dispatcher'
        service_class = object
        service_port = 4444
        with self.assertRaisesRegex(AssertionError, service_name) and \
                self.assertRaisesRegex(AttributeError, 'object'):
            helpers.start_common_server(service_name, service_class, service_port)

    @mock.patch('helpers.INFINITE_LOOP', False)
    def test_start_common_server_for_service_from_proto(self):
        service_name = 'Dispatcher'
        my_dispatcher = mock.MagicMock()
        service_class = my_dispatcher
        service_port = 4444
        self.assertFalse(helpers.start_common_server(service_name, service_class, service_port))

    def test_get_random_agents_with_parity(self):
        agents_idx = helpers.get_random_agents(10, rate=0.1, parity=True)
        self.assertEqual(len(agents_idx), 2)

    def test_get_random_agents_without_parity(self):
        agents_idx = helpers.get_random_agents(10, rate=0.1, parity=False)
        self.assertEqual(len(agents_idx), 1)

    def test_get_random_agents_with_max_rate_and_without_parity(self):
        agents_idx = helpers.get_random_agents(10, rate=1.2, parity=False)
        self.assertEqual(len(agents_idx), 10)

    def test_get_random_agents_with_zero_rate_and_parity(self):
        agents_idx = helpers.get_random_agents(10, rate=0, parity=True)
        self.assertEqual(len(agents_idx), 0)

    def test_get_two_different_randints_for_two_possibilities(self):
        radints = helpers.get_two_different_randints(0, 1)
        self.assertCountEqual(radints, [0, 1])

    def test_get_two_different_randints_for_no_possibilities(self):
        with self.assertRaisesRegex(ValueError, r"Sample larger than population or is negative"):
            _ = helpers.get_two_different_randints(0, -1)

    def test_get_two_different_randints_for_one_possibility(self):
        radints = helpers.get_two_different_randints(0, 0)
        self.assertEqual(radints, [0, 0])

class TestHelpersGrpcCall(unittest.TestCase):

    port = 1110
    test_message = "test_message"
    request_type = helpers.proto.settler_pb2.GeneticOperatorRequest
    response_type = helpers.proto.settler_pb2.JobId
    stub_class = helpers.proto.settler_pb2_grpc.DispatcherStub
    job_id = '5'

    def setUp(self):
        class Dispatcher(helpers.proto.settler_pb2_grpc.DispatcherServicer): #pylint: disable=too-few-public-methods
            def HandleJobRequest(self, request, context):
                return helpers.proto.settler_pb2.JobId(job_id='5')

        self.dummy_class = Dispatcher
        self.dummy_method_name = 'HandleJobRequest'
        self.server = helpers.grpc.server(
            helpers.futures.ThreadPoolExecutor(max_workers=10))
        helpers.proto.settler_pb2_grpc.add_DispatcherServicer_to_server(
            self.dummy_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    def test_grpc_sync_call(self):
        response = helpers.grpc_sync_call(
            self.request_type(), 'localhost', self.port, self.stub_class, self.dummy_method_name)
        self.assertEqual(response.job_id, self.job_id)

    def test_grpc_sync_call_logging(self):
        with self.assertLogs(level='DEBUG') as log:
            _ = helpers.grpc_sync_call(
                self.request_type(),
                'localhost',
                self.port,
                self.stub_class,
                self.dummy_method_name)
        self.assertEqual(len(log.records), 2)

    def test_grpc_async_call_with_callback(self):
        with helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            _ = helpers.grpc_async_call(
                self.request_type(),
                channel,
                self.stub_class,
                self.dummy_method_name,
                lambda call_future: self.assertEqual(call_future.result().job_id, self.job_id))

    def test_grpc_async_call_without_callback(self):
        with helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            future_response = helpers.grpc_async_call(
                self.request_type(), channel, self.stub_class, self.dummy_method_name)
        self.assertRegex(str(future_response), 'StatusCode.CANCELLED')

    def test_grpc_async_call_logging(self):
        with self.assertLogs(level='DEBUG') as log:
            with helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
                _ = helpers.grpc_async_call(
                    self.request_type(),
                    channel,
                    self.stub_class,
                    self.dummy_method_name)
            self.assertEqual(len(log.records), 2)


class TestHelpersGrpcCallWithSleep(unittest.TestCase):

    port = 1110
    test_message = "test_message"
    request_type = helpers.proto.settler_pb2.GeneticOperatorRequest
    response_type = helpers.proto.settler_pb2.JobId
    stub_class = helpers.proto.settler_pb2_grpc.DispatcherStub
    sleep_time = 2

    def setUp(self):
        class Dispatcher(helpers.proto.settler_pb2_grpc.DispatcherServicer): #pylint: disable=too-few-public-methods
            def HandleJobRequest(self, request, context):
                time.sleep(2)
                return helpers.proto.settler_pb2.JobId()

        self.dummy_class = Dispatcher
        self.dummy_method_name = 'HandleJobRequest'
        self.server = helpers.grpc.server(
            helpers.futures.ThreadPoolExecutor(max_workers=10))
        helpers.proto.settler_pb2_grpc.add_DispatcherServicer_to_server(
            self.dummy_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    def test_grpc_sync_call(self):
        start = time.time()
        _ = helpers.grpc_sync_call(
            self.request_type(), 'localhost', self.port, self.stub_class, self.dummy_method_name)
        end = time.time()
        self.assertAlmostEqual(end - start, self.sleep_time, places=1)

    def test_grpc_async_call(self):
        num_of_executions = 4
        with helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            start = time.time()
            for _ in range(num_of_executions):
                _ = helpers.grpc_async_call(
                    self.request_type(),
                    channel,
                    self.stub_class,
                    self.dummy_method_name)
            end = time.time()
            self.assertAlmostEqual(end - start, 0, places=1)

    def test_wait_for_all(self):
        num_of_executions = 4
        with helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            start = time.time()
            helpers.wait_for_all([
                helpers.grpc_async_call(
                    self.request_type(),
                    channel,
                    self.stub_class,
                    self.dummy_method_name)
                for _
                in range(num_of_executions)])
            end = time.time()
            self.assertAlmostEqual(end - start, self.sleep_time, places=1)

class TestUndirectedGraph(unittest.TestCase): #pylint: disable=too-many-instance-attributes

    def setUp(self):
        self.p0 = helpers.proto.settler_pb2.Person(name="unknown")
        self.p1 = helpers.proto.settler_pb2.Person(name="v1")
        self.p2 = helpers.proto.settler_pb2.Person(name="v2")
        self.v1 = helpers.proto.settler_pb2.Vertex(label="v1", id=1)
        self.v2 = helpers.proto.settler_pb2.Vertex(label="v2", id=2)
        self.v3 = helpers.proto.settler_pb2.Vertex(label="v3", id=1)
        self.v4 = helpers.proto.settler_pb2.Vertex(label="v4", id=2)
        self.e1 = helpers.proto.settler_pb2.Edge(vertex1=self.v1, vertex2=self.v2)
        self.graph = helpers.proto.settler_pb2.UndirectedGraph(
            vertices=[self.v1, self.v2, self.v3, self.v4],
            edges=[self.e1])

    def test_get_edge_by_name(self):
        edge = helpers.get_edge_by_name(self.graph, self.p1, self.p2)
        self.assertEqual(edge, self.e1)

    def test_get_edge_by_name_rotated_edge(self):
        edge = helpers.get_edge_by_name(self.graph, self.p2, self.p1)
        self.assertEqual(edge, self.e1)

    def test_get_edge_by_name_unknown_edge(self):
        with self.assertRaises(IndexError):
            helpers.get_edge_by_name(self.graph, self.p0, self.p1)

    def test_get_edge_by_id(self):
        edge = helpers.get_edge_by_id(self.graph, self.v3, self.v4)
        self.assertEqual(edge, self.e1)

    def test_get_edge_by_id_rotated_edge(self):
        edge = helpers.get_edge_by_id(self.graph, self.v4, self.v3)
        self.assertEqual(edge, self.e1)

    def test_get_edge_by_id_unknown_edge(self):
        with self.assertRaises(IndexError):
            helpers.get_edge_by_id(self.graph, self.v1, self.v3)
