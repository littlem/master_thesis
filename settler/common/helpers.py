#!/usr/bin/env python
#pylint: disable=missing-docstring

from concurrent import futures
from typing import Tuple
from random import shuffle, sample
import time
import logging
import os

import grpc
import proto.settler_pb2_grpc


logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('common/helpers')
_ONE_DAY_IN_SECONDS = 60 * 60 * 24
INFINITE_LOOP = True

class MissingEnvironmentVariableException(Exception):
    def __init__(self, variable_name):
        super().__init__(f"'{variable_name}' enviornment variable is unreachable")


def verify_endpoint(service_name):
    try:
        os.environ[f'{service_name}_HOSTNAME']
    except KeyError:
        raise MissingEnvironmentVariableException(f'{service_name}_HOSTNAME')
    try:
        os.environ[f'{service_name}_PORT']
    except KeyError:
        raise MissingEnvironmentVariableException(f'{service_name}_PORT')

def get_endpoint(service_name):
    return os.environ[f'{service_name}_HOSTNAME'], os.environ[f'{service_name}_PORT']

def start_common_server(service_name, service_class, service_port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    getattr(
        proto.settler_pb2_grpc, f'add_{service_name}Servicer_to_server')(service_class(), server)
    server.add_insecure_port(f'[::]:{service_port}')
    server.start()
    try:
        while INFINITE_LOOP:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        LOGGER.info("KeyboardInterrupt detected - closing gRPC server.")
    finally:
        server.stop(0)

def grpc_sync_call(request, hostname, port, stub_class, method_name):
    LOGGER.debug("Sync request to service on %s:%d", hostname, port)
    with grpc.insecure_channel(f'{hostname}:{port}') as channel:
        stub = stub_class(channel)
        response = getattr(stub, method_name)(request)
        LOGGER.debug("Result is '%s'", response)
        return response

def grpc_async_call(request, channel, stub_class, method_name, callback=None):
    LOGGER.debug("Async request to service on channel %s", channel)
    stub = stub_class(channel)
    future_response = getattr(stub, method_name).future(request)
    LOGGER.debug("Return response future '%s'", future_response)
    if callback:
        future_response.add_done_callback(callback)
    return future_response

def wait_for_all(tuple_of_futures: Tuple[grpc.Future]) -> list:
    while True:
        for future in tuple_of_futures:
            if not future.done():
                break
        return [future.result() for future in tuple_of_futures]

def get_edge_by_id(graph: proto.settler_pb2.UndirectedGraph, \
        ver1: proto.settler_pb2.Vertex, ver2: proto.settler_pb2.Vertex) -> proto.settler_pb2.Edge:
    return list(filter(lambda edge: (edge.vertex1.id == ver1.id and edge.vertex2.id == ver2.id) or \
        (edge.vertex2.id == ver1.id and edge.vertex1.id == ver2.id), graph.edges))[0]

def get_edge_by_name(graph: proto.settler_pb2.UndirectedGraph, \
        per1: proto.settler_pb2.Person, per2: proto.settler_pb2.Person) -> proto.settler_pb2.Edge:
    return list(filter(lambda edge: \
        (edge.vertex1.label == per1.name and edge.vertex2.label == per2.name) or \
        (edge.vertex2.label == per1.name and edge.vertex1.label == per2.name), graph.edges))[0]

def get_random_agents(agents_range, rate, parity=False):
    all_agents = list(range(agents_range))
    shuffle(all_agents)
    quantity_of_agents = int(rate*agents_range)
    if rate == 0:
        return []
    if quantity_of_agents == 0:
        quantity_of_agents = 1
    elif quantity_of_agents > agents_range:
        quantity_of_agents = agents_range

    if parity and quantity_of_agents % 2:
        if quantity_of_agents == agents_range:
            quantity_of_agents -= 1
        return all_agents[:(quantity_of_agents + 1)]
    return all_agents[:quantity_of_agents]

def get_two_different_randints(minimum, maximum):
    if minimum == maximum:
        return [minimum, minimum]
    return sample(range(minimum, maximum + 1), 2)
