#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock

import common.helpers
import mutate_operator

class TestMutateOperator(unittest.TestCase):
    server_class = mutate_operator.MutateOperator
    port = 1111
    test_message = "test_message_for_mutate_operator"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_MutateOperatorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()
        ver1 = common.helpers.proto.settler_pb2.Vertex(label="A", x=0.5, y=0.5)
        ver2 = common.helpers.proto.settler_pb2.Vertex(label="B", x=0.0, y=0.0)
        ver3 = common.helpers.proto.settler_pb2.Vertex(label="C", x=0.0, y=0.0)
        self.per1 = common.helpers.proto.settler_pb2.Person(name=ver1.label)
        self.per2 = common.helpers.proto.settler_pb2.Person(name=ver2.label)
        self.per3 = common.helpers.proto.settler_pb2.Person(name=ver3.label)
        self.chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2, self.per3])
        self.mutate_request = mutate_operator.GeneticOperatorRequest(
            population=common.helpers.proto.settler_pb2.Population(
                chromosommes=[self.chromosomme]),
            rate=0.5)

    def tearDown(self):
        self.server.stop(None)

    def test_produce_mutated_population_for_empty(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.MutateOperatorStub(channel)
            empty_population = stub.ProduceMutatedPopulation(
                mutate_operator.GeneticOperatorRequest())
            self.assertEqual(len(empty_population.chromosommes), 0)

    def test_produce_mutated_population_len(self):
        population_length = 10
        for _ in range(population_length):
            self.mutate_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(
                    order=[self.per1, self.per2, self.per3])])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.MutateOperatorStub(channel)
            population = stub.ProduceMutatedPopulation(self.mutate_request)
            self.assertEqual(len(population.chromosommes), population_length + 1)


    @mock.patch('mutate_operator.get_random_agents')
    @mock.patch('mutate_operator.mutate_chromosomme')
    def test_produce_mutated_population_for_same_elements(self, \
            mutate_chromosomme_mock, get_random_agents_mock):
        population_length = 10
        mutated_idx = [0, 1, 5]
        mutated_chromosomme = common.helpers.proto.settler_pb2.Chromosomme(order=[])
        get_random_agents_mock.return_value = mutated_idx
        mutate_chromosomme_mock.return_value = mutated_chromosomme
        for _ in range(population_length):
            self.mutate_request.population.chromosommes.extend([
                common.helpers.proto.settler_pb2.Chromosomme(order=self.chromosomme.order)])
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.MutateOperatorStub(channel)
            population = stub.ProduceMutatedPopulation(self.mutate_request)
            for i, chromosomme in enumerate(population.chromosommes):
                with self.subTest(chromosmme_index=i):
                    self.assertEqual(
                        chromosomme.order,
                        mutated_chromosomme.order if i in mutated_idx else self.chromosomme.order)

    def test_produce_mutated_population_logging(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.MutateOperatorStub(channel)
                _ = stub.ProduceMutatedPopulation(mutate_operator.GeneticOperatorRequest())
            self.assertEqual(len(log.records), 2)

    @mock.patch('mutate_operator.get_two_different_randints', return_value=(0, 1))
    def test_mutate_chromosomme(self, _):
        chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per1, self.per2, self.per3])
        mutated_chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[self.per2, self.per1, self.per3])
        self.assertEqual(
            mutate_operator.mutate_chromosomme(chromosomme).order,
            mutated_chromosomme.order)

    def test_mutate_chromosomme_for_empty(self):
        chromosomme = common.helpers.proto.settler_pb2.Chromosomme(
            order=[])
        self.assertEqual(
            mutate_operator.mutate_chromosomme(chromosomme).order,
            chromosomme.order)
