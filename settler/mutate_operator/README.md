# Mutate Operator
*(pol. Operator mutacji)*

## Responsibilities
- generating new population based on given population using gene transposition mutation (from Genetic Algorithms theory)

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/mutate_operator
sudo docker build -t mutate_operator .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 mutate_operator # other ports could also be parsed here
```
