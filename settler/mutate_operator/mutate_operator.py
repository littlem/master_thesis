#!/usr/bin/env python
#pylint: disable=missing-docstring,too-many-arguments

import logging
import argparse
from copy import deepcopy

from proto.settler_pb2_grpc import MutateOperatorServicer
from proto.settler_pb2 import Population, Chromosomme, GeneticOperatorRequest #pylint: disable=unused-import
from common.helpers import start_common_server, get_random_agents, get_two_different_randints


logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('mutate_operator')

def mutate_chromosomme(agent: Chromosomme) -> Chromosomme:
    chromosomme = list(deepcopy(agent.order))
    try:
        i, j = get_two_different_randints(0, len(chromosomme) - 1) # pylint: disable=unbalanced-tuple-unpacking
        chromosomme[i], chromosomme[j] = chromosomme[j], chromosomme[i]
        return Chromosomme(order=chromosomme)
    except ValueError:
        return agent


class MutateOperator(MutateOperatorServicer): #pylint: disable=too-few-public-methods

    def ProduceMutatedPopulation(self, request: GeneticOperatorRequest, context):
        LOGGER.debug("New population based on genetic operator request %s", request)
        population = list(request.population.chromosommes)
        # get random agents to mutate
        agents_idx = get_random_agents(len(population), request.rate, parity=False)
        LOGGER.debug("These agents will be mutated %s", agents_idx)
        # order crossover perform and add to new population
        for idx in agents_idx:
            population[idx] = mutate_chromosomme(population[idx])
        return Population(chromosommes=population)

def get_args():
    parser = argparse.ArgumentParser(description='Mutate Operator for genetic algorithm.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for mutate_operator to run')
    return parser.parse_args()

def main():
    args = get_args()
    start_common_server('MutateOperator', MutateOperator, args.port)


if __name__ == '__main__':
    main()
