# coding: utf-8

from __future__ import absolute_import

import os
import importlib.util
from flask import json
from six import BytesIO
from unittest import mock

import swagger_server.controllers.job_controller
from swagger_server.models.job import Job
from swagger_server.models.job_result import JobResult
from swagger_server.models.point import Point
from swagger_server.models.table import Table
from swagger_server.test import BaseTestCase

from http import HTTPStatus

class TestJobControllerWithoutDispatcher(BaseTestCase):

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    def test_add_job(self, *_):
        body = Job(map=[Table(name="t1", points=[Point(x=0.0, y=1.1)])], persons=["A"])
        response = self.client.open(
            '/api/job',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        status_code = HTTPStatus.SERVICE_UNAVAILABLE
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    def test_get_job_by_id(self, *_):
        response = self.client.open(
            '/api/job/{jobId}'.format(jobId=789),
            method='GET')
        status_code = HTTPStatus.SERVICE_UNAVAILABLE
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())


class TestJobControllerWithDispatcher(BaseTestCase):
    test_message = "test_message"
    mocked_environ = {
        'DISPATCHER_HOSTNAME': 'tmp',
        'DISPATCHER_PORT': 'tmp'
    }
    mocked_job = swagger_server.controllers.job_controller.JobId(job_id=test_message)

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    @mock.patch('swagger_server.controllers.os.environ')
    def test_add_job(self, environ_mock, dispatcher_stub_mock, _):
        environ_mock.return_value = self.mocked_environ
        dispatcher_stub_mock().HandleJobRequest.return_value = self.mocked_job
        body = Job(map=[Table(name="t1", points=[Point(x=0.0, y=1.1)])], persons=["A"])
        response = self.client.open(
            '/api/job',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        status_code = HTTPStatus.ACCEPTED
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    @mock.patch('swagger_server.controllers.os.environ')
    def test_add_job_response_content(self, environ_mock, dispatcher_stub_mock, _):
        environ_mock.return_value = self.mocked_environ
        dispatcher_stub_mock().HandleJobRequest.return_value = self.mocked_job
        body = Job(map=[Table(name="t1", points=[Point(x=0.0, y=1.1)])], persons=["A"])
        response = self.client.open(
            '/api/job',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assertTrue(self.test_message in response.data.decode('UTF-8'))

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    @mock.patch('swagger_server.controllers.os.environ')
    def test_get_job_by_id_in_progress(self, environ_mock, dispatcher_stub_mock, _):
        environ_mock.return_value = self.mocked_environ
        mocked_result = swagger_server.controllers.job_controller.AlgorithmResult(
            status=swagger_server.controllers.job_controller.AlgorithmResult.IN_PROGRESS)
        dispatcher_stub_mock().GetJobResult.return_value = mocked_result
        response = self.client.open(
            '/api/job/{jobId}'.format(jobId=789),
            method='GET')
        status_code = HTTPStatus.ACCEPTED
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    @mock.patch('swagger_server.controllers.os.environ')
    def test_get_job_by_id_in_progress(self, environ_mock, dispatcher_stub_mock, _):
        environ_mock.return_value = self.mocked_environ
        mocked_result = swagger_server.controllers.job_controller.AlgorithmResult(
            status=swagger_server.controllers.job_controller.AlgorithmResult.NON_EXISTANT)
        dispatcher_stub_mock().GetJobResult.return_value = mocked_result
        response = self.client.open(
            '/api/job/{jobId}'.format(jobId=789),
            method='GET')
        status_code = HTTPStatus.NOT_FOUND
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())

    @mock.patch('swagger_server.controllers.job_controller.LOGGER')
    @mock.patch('swagger_server.controllers.job_controller.DispatcherStub')
    @mock.patch('swagger_server.controllers.os.environ')
    def test_get_job_by_id_in_progress(self, environ_mock, dispatcher_stub_mock, _):
        environ_mock.return_value = self.mocked_environ
        mocked_result = swagger_server.controllers.job_controller.AlgorithmResult(
            status=swagger_server.controllers.job_controller.AlgorithmResult.READY,
            result=None)
        dispatcher_stub_mock().GetJobResult.return_value = mocked_result
        response = self.client.open(
            '/api/job/{jobId}'.format(jobId=789),
            method='GET')
        status_code = HTTPStatus.OK
        self.assertEqual(response.status, f"{status_code.value} {status_code.phrase}".upper())


if __name__ == '__main__':
    import unittest
    unittest.main()
