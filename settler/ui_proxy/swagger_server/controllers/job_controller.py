import logging
import os
import connexion
import six


import grpc

from proto.settler_pb2 import AlgorithmParams, JobId, AlgorithmResult
from proto.settler_pb2_grpc import DispatcherStub

from swagger_server.models.job import Job
from swagger_server.models.job_result import JobResult
from swagger_server.models.settler_response import SettlerResponse
from swagger_server.models.settler_response_with_job_id import SettlerResponseWithJobId
from swagger_server.models.settler_response_with_job_result import SettlerResponseWithJobResult
from swagger_server import util
from swagger_server.controllers import table2repeated_vertex, person_and_prio2person, \
    point2vertex, relation2relation, get_endpoint, settlerResponse, settled_person_list2result

from http import HTTPStatus

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('ui_proxy/job_controller')

def add_job(body):
    """Register new job to perform SAP algorithm

    :param body: Job object that needs to be created with given algorithm&#39;s parameters
    :type body: dict | bytes

    :rtype: SettlerResponse
    """
    if connexion.request.is_json:
        body = Job.from_dict(connexion.request.get_json())
        dispatcher_hostname, dispatcher_port = get_endpoint()
        if isinstance(dispatcher_hostname, SettlerResponse):
            return dispatcher_hostname, dispatcher_port
        LOGGER.debug("Dispatcher's endpoint is '%s:%d'", dispatcher_hostname, dispatcher_port)
        try:
            with grpc.insecure_channel(f'{dispatcher_hostname}:{dispatcher_port}') as channel:
                stub = DispatcherStub(channel)
                http_message = stub.HandleJobRequest(AlgorithmParams(
                    map=table2repeated_vertex(body.map),
                    persons=[person_and_prio2person(person, body.priorities) for person in body.persons],
                    relations=[relation2relation(relation) for relation in body.relations] if body.relations else [],
                    max_priority=point2vertex(body.max_priority),
                    default_weight=body.default_weight,
                    default_priority=body.default_priority,
                    cross_rate=body.cross_rate,
                    mutation_rate=body.mutation_rate,
                    population_quantity=body.population_quantity,
                    iteration_max=body.iteration_max,
                    relations_rate=body.relations_rate))
                return SettlerResponseWithJobId(
                    title=HTTPStatus.ACCEPTED.phrase,
                    status=HTTPStatus.ACCEPTED.value,
                    detail="New job registered to be performed",
                    job_id=http_message.job_id), HTTPStatus.ACCEPTED.value
        except Exception as e:
            return settlerResponse(HTTPStatus.SERVICE_UNAVAILABLE, str(e.details))
    LOGGER.error("Received strange job request which aren't json %s", connexion.request)
    return settlerResponse(HTTPStatus.BAD_REQUEST, 'Invalid non-JSON input format')

def get_job_by_id(jobId):
    """Find job&#39;s result/status

    Returns a result for given job # noqa: E501

    :param jobId: ID of job to return
    :type jobId: int

    :rtype: JobResult|SettlerResponse
    """
    dispatcher_hostname, dispatcher_port = get_endpoint()
    if isinstance(dispatcher_hostname, SettlerResponse):
        return dispatcher_hostname, dispatcher_port
    LOGGER.debug("Dispatcher's endpoint is '%s:%d'", dispatcher_hostname, dispatcher_port)
    try:
        with grpc.insecure_channel(f'{dispatcher_hostname}:{dispatcher_port}') as channel:
            stub = DispatcherStub(channel)
            LOGGER.debug("Perform request for job_id '%s'", jobId)
            algorithm_result = stub.GetJobResult(JobId(job_id=jobId))
            LOGGER.debug("Received algorithm result '%s'", algorithm_result)
            if algorithm_result.status == AlgorithmResult.READY:
                return SettlerResponseWithJobResult(
                    title=HTTPStatus.OK.phrase,
                    status=HTTPStatus.OK.value,
                    detail="New job registered to be performed",
                    job_result=settled_person_list2result(algorithm_result.result))
            elif algorithm_result.status == AlgorithmResult.NON_EXISTANT:
                return settlerResponse(HTTPStatus.NOT_FOUND, f"Job with id '{jobId}' not found.")
            elif algorithm_result.status == AlgorithmResult.IN_PROGRESS:
                return settlerResponse(HTTPStatus.ACCEPTED, f"Job with id '{jobId}' still in progress.")
            else:
                return settlerResponse(HTTPStatus.INTERNAL_SERVER_ERROR, 'Who knows, what happened')
    except Exception as e:
        return settlerResponse(HTTPStatus.SERVICE_UNAVAILABLE, str(e.details))
