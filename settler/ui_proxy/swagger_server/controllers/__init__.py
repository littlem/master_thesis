"""
Transcoding REST definitions (Swagger) to Protocol Buffers messages
"""

import os
from proto.settler_pb2 import Vertex, Person, Relation
from swagger_server.models.settler_response import SettlerResponse
from swagger_server.models.table import Table
from swagger_server.models.point import Point
from http import HTTPStatus

def point2vertex(point):
    if not point:
        return Vertex(x=0.0, y=0.0)
    return Vertex(x=point.x, y=point.y)

def person_and_prio2person(person_name, priorities):
    if priorities:
        person_priority = list(filter(lambda p: p.name == person_name, priorities))
        if person_priority:
            return Person(name=person_name, priority=person_priority[0].priority)
    return Person(name=person_name)

def relation2relation(relation):
    return Relation(to=relation.to, weight=relation.weight, fr=relation.fr)

def table2repeated_vertex(map_of_tables):
    repeated_vertex = []
    for table in map_of_tables:
        for point in table.points:
            vertex = point2vertex(point)
            vertex.label = table.name
            repeated_vertex.append(vertex)
    return repeated_vertex

"""
Return typical response
"""
def settlerResponse(httpStatus, detail):
    return SettlerResponse(title=httpStatus.phrase, detail=detail, type=httpStatus.value), httpStatus.value

"""
Verifying related microservices' endpoints
"""
def get_endpoint(service_name='DISPATCHER'):
    try:
        dispatcher_hostname = os.environ[f'{service_name}_HOSTNAME']
    except KeyError:
        return settlerResponse(
            HTTPStatus.SERVICE_UNAVAILABLE,
            f"{service_name}_HOSTNAME' enviornment variable is unreachable")
    try:
        dispatcher_port = os.environ[f'{service_name}_PORT']
    except KeyError:
        return settlerResponse(
            HTTPStatus.SERVICE_UNAVAILABLE,
            f"{service_name}_PORT' enviornment variable is unreachable")
    return dispatcher_hostname, dispatcher_port

def settled_person_list2result(settledPersonList):
    tables_raw = dict()
    for settledPerson in settledPersonList:
        tables_raw.setdefault(settledPerson.vertex.label, []).append(Point(
            x=settledPerson.vertex.x,
            y=settledPerson.vertex.y,
            label=settledPerson.person.name))
    return [
        Table(name=table_name, points=table_points)
        for table_name, table_points
        in tables_raw.items()]
