# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server.models.settler_response import SettlerResponse  # noqa: F401,E501
from swagger_server import util


class SettlerResponseWithJobId(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    def __init__(self, title: str=None, detail: str=None, type: str='about:blank', status: int=None, job_id: str=None):  # noqa: E501
        """SettlerResponseWithJobId - a model defined in Swagger

        :param title: The title of this SettlerResponseWithJobId.  # noqa: E501
        :type title: str
        :param detail: The detail of this SettlerResponseWithJobId.  # noqa: E501
        :type detail: str
        :param type: The type of this SettlerResponseWithJobId.  # noqa: E501
        :type type: str
        :param status: The status of this SettlerResponseWithJobId.  # noqa: E501
        :type status: int
        :param job_id: The job_id of this SettlerResponseWithJobId.  # noqa: E501
        :type job_id: str
        """
        self.swagger_types = {
            'title': str,
            'detail': str,
            'type': str,
            'status': int,
            'job_id': str
        }

        self.attribute_map = {
            'title': 'title',
            'detail': 'detail',
            'type': 'type',
            'status': 'status',
            'job_id': 'job_id'
        }

        self._title = title
        self._detail = detail
        self._type = type
        self._status = status
        self._job_id = job_id

    @classmethod
    def from_dict(cls, dikt) -> 'SettlerResponseWithJobId':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The SettlerResponseWithJobId of this SettlerResponseWithJobId.  # noqa: E501
        :rtype: SettlerResponseWithJobId
        """
        return util.deserialize_model(dikt, cls)

    @property
    def title(self) -> str:
        """Gets the title of this SettlerResponseWithJobId.


        :return: The title of this SettlerResponseWithJobId.
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title: str):
        """Sets the title of this SettlerResponseWithJobId.


        :param title: The title of this SettlerResponseWithJobId.
        :type title: str
        """
        if title is None:
            raise ValueError("Invalid value for `title`, must not be `None`")  # noqa: E501

        self._title = title

    @property
    def detail(self) -> str:
        """Gets the detail of this SettlerResponseWithJobId.


        :return: The detail of this SettlerResponseWithJobId.
        :rtype: str
        """
        return self._detail

    @detail.setter
    def detail(self, detail: str):
        """Sets the detail of this SettlerResponseWithJobId.


        :param detail: The detail of this SettlerResponseWithJobId.
        :type detail: str
        """

        self._detail = detail

    @property
    def type(self) -> str:
        """Gets the type of this SettlerResponseWithJobId.


        :return: The type of this SettlerResponseWithJobId.
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type: str):
        """Sets the type of this SettlerResponseWithJobId.


        :param type: The type of this SettlerResponseWithJobId.
        :type type: str
        """

        self._type = type

    @property
    def status(self) -> int:
        """Gets the status of this SettlerResponseWithJobId.


        :return: The status of this SettlerResponseWithJobId.
        :rtype: int
        """
        return self._status

    @status.setter
    def status(self, status: int):
        """Sets the status of this SettlerResponseWithJobId.


        :param status: The status of this SettlerResponseWithJobId.
        :type status: int
        """
        if status is None:
            raise ValueError("Invalid value for `status`, must not be `None`")  # noqa: E501

        self._status = status

    @property
    def job_id(self) -> str:
        """Gets the job_id of this SettlerResponseWithJobId.


        :return: The job_id of this SettlerResponseWithJobId.
        :rtype: str
        """
        return self._job_id

    @job_id.setter
    def job_id(self, job_id: str):
        """Sets the job_id of this SettlerResponseWithJobId.


        :param job_id: The job_id of this SettlerResponseWithJobId.
        :type job_id: str
        """
        if job_id is None:
            raise ValueError("Invalid value for `job_id`, must not be `None`")  # noqa: E501

        self._job_id = job_id
