#!/usr/bin/env python3
#pylint: disable=missing-docstring

import argparse
import connexion

from swagger_server import encoder

def get_args():
    parser = argparse.ArgumentParser(description='UI proxy for transcoding.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for app to run')
    return parser.parse_args()

def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Settler'})
    args = get_args()
    app.run(port=args.port)


if __name__ == '__main__':
    main()
