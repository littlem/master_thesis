# Population Generator
*(pol. Generator populacji)*

## Responsibilities
- generating population based on given chromosomme (combined) with requested quantity

## Build & run a docker image

```bash
git clone git@gitlab.com:littlem/master_thesis.git
cd master_thesis/settler/population_generator
sudo docker build -t population_generator .
sudo docker run -it -p 8080:8070 --env APP_PORT=8070 population_generator # other ports could also be parsed here
```
