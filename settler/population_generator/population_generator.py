#!/usr/bin/env python
#pylint: disable=missing-docstring

import logging
import argparse
import random
from copy import deepcopy

from proto.settler_pb2_grpc import PopulationGeneratorServicer
from proto.settler_pb2 import Population, Chromosomme, PopulationRequest, Person #pylint: disable=unused-import
from common.helpers import start_common_server

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('population_generator')

class PopulationGenerator(PopulationGeneratorServicer): #pylint: disable=too-few-public-methods
    def ProducePopulation(self, request, context):
        LOGGER.debug(
            "New population based on %s with quantity %d", request.chromosomme, request.quantity)
        new_chromosommes = []
        for _ in range(request.quantity):
            new_chromosomme = list(deepcopy(request.chromosomme.order))
            random.shuffle(new_chromosomme)
            new_chromosommes.append(new_chromosomme)
        population = [
            Chromosomme(order=new_chromosomme)
            for new_chromosomme
            in new_chromosommes]
        return Population(chromosommes=population)

def get_args():
    parser = argparse.ArgumentParser(description='Population Generator for genetic algorithm.')
    parser.add_argument('--port', '-p', required=True,
                        help='Port for population_generator to run')
    return parser.parse_args()

def main():
    args = get_args()
    start_common_server('PopulationGenerator', PopulationGenerator, args.port)


if __name__ == '__main__':
    main()
