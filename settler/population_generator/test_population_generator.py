#!/usr/bin/env python
#pylint: disable=missing-docstring

import unittest
from unittest import mock

import common.helpers
import population_generator

class TestPopulationGenerator(unittest.TestCase):
    server_class = population_generator.PopulationGenerator
    port = 1111
    test_message = "test_message_for_population_generator"

    def setUp(self):
        self.server = common.helpers.grpc.server(
            common.helpers.futures.ThreadPoolExecutor(max_workers=10))
        common.helpers.proto.settler_pb2_grpc.add_PopulationGeneratorServicer_to_server(
            self.server_class(), self.server)
        self.server.add_insecure_port(f'[::]:{self.port}')
        self.server.start()

    def tearDown(self):
        self.server.stop(None)

    def test_produce_population(self):
        chromosomme = population_generator.Chromosomme(order=[
            population_generator.Person(name=name)
            for name
            in ['A', 'B', 'C', 'D']])
        quantity = 4
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.PopulationGeneratorStub(channel)
            response = stub.ProducePopulation(
                population_generator.PopulationRequest(chromosomme=chromosomme, quantity=quantity))
        self.assertEqual(len(response.chromosommes), quantity)

    @mock.patch('population_generator.random.shuffle')
    def test_produce_population_shuffle(self, shuffle_mock):
        chromosomme = population_generator.Chromosomme(order=[
            population_generator.Person(name=name)
            for name
            in ['A', 'B', 'C', 'D']])
        quantity = 4
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            stub = common.helpers.proto.settler_pb2_grpc.PopulationGeneratorStub(channel)
            _ = stub.ProducePopulation(
                population_generator.PopulationRequest(chromosomme=chromosomme, quantity=quantity))
        self.assertEqual(shuffle_mock.call_count, quantity)
        shuffle_mock.assert_called_with(list(chromosomme.order))

    def test_produce_population_logging(self):
        with common.helpers.grpc.insecure_channel(f'localhost:{self.port}') as channel:
            with self.assertLogs(level='DEBUG') as log:
                stub = common.helpers.proto.settler_pb2_grpc.PopulationGeneratorStub(channel)
                _ = stub.ProducePopulation(population_generator.PopulationRequest())
            self.assertEqual(len(log.records), 1)
