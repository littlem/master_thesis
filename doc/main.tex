\chapter{Aplikacja}\label{cha:main}

\section{Prezentacja aplikacji}

\subsection{Diagram komponentów}
\label{subsec:diagram_komponentow}

Jak wspomniano we wstępie \ref{cha:wstep}, stworzona aplikacja \textbf{Settler} rozwiązuje problem przydziału miejsc przy stołach dla zadanych parametrów wejściowych. Specyfika tego problemu, będącego już przedmiotem pracy inżynierskiej \cite{praca_inzynierska}, skłoniła bezpośrednio do zaimplementowania aplikacji w oparciu o architekturę mikrousług. Aby solidnie umotywować ten wybór, należy przyjrzeć się strukturze rozwiązania - algorytmowi genetycznemu.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{images/diagram1.pdf}
	\caption{Kroki algorytmu genetycznego.}
	\small\textsuperscript{Źródło: \cite{praca_inzynierska}}
	\label{rys:algorithm_steps}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/parallel_algorithm_steps.pdf}
	\caption{Kroki algorytmu genetycznego realizowane przez aplikację Settler.}
	\label{rys:parallel_algorithm_steps}
\end{figure}

Jak pokazuje schemat \ref{rys:algorithm_steps}, kroki tego algorytmu inspirowanego naturą skupiają się na kilku działaniach przeprowadzanych na (początkowo losowo) wygenerowanej populacji. Działania te nazywane są operatorami genetycznymi. Selekcja, krzyżowanie oraz mutacja, bo o nich tu mowa, stanowią kolejne etapy ewolucji prowadzące do znalezienia optymalnego rozwiązania. Cykl kończy obliczenie funkcji celu, by móc znaleźć i zapisać najlepszy wynik osobnika danej populacji. Każda z tych operacji może być rozumiana jako osobna usługa (mikrousługa) operująca na zadanych parametrach - populacji, bądź też jej specyfikacji w przypadku generatora populacji.

Ze względu na to, że operatory genetyczne operują na całej populacji (modyfikują ją) to jest to działanie kosztowne obliczeniowo. Dokonano więc zrównoleglenia niektórych części algorytmu zgodnie ze schematem \ref{rys:parallel_algorithm_steps}. Po prawej stronie widoczne są nazwy mikrousług odpowiedzialne za daną część operacji.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{images/components_diagram.pdf}
	\caption{Diagram komponentów aplikacji Settler.}
	\label{rys:components_diagram}
\end{figure}

Ostatecznie architekturę aplikacji można zobaczyć na schemacie  \ref{rys:components_diagram}. Wszystkie mikrousługi zostały napisane w języku Python, a komunikacja między nimi została zaimplementowana w oparciu o gRPC. By ułatwić użytkownikom korzystanie z aplikacji, za pomocą usługi \textbf{ui\_proxy} dokonywana jest translacja rezultatów działania aplikacji na REST. W drugim kierunku natomiast dokonywane jest tłumaczenie zapytań na gRPC. Komunikacja oraz powstałe problemy implementacyjne zostaną omówione w podsekcji \ref{subsec:komunikacja_aplikacja}.

\textbf{Dyspozytor} (\textbf{dispatcher}) jest odpowiedzialny za odbiór zapytania użytkownika oraz zareagowanie na nie. W zależności od typu zapytania, dyspozytor odpytuje bazę danych o rezultat danego zadania (zgodnie z diagramem interakcji \ref{rys:sequence_user_result}), bądź też dla zadanych parametrów algorytmu tworzy nowe zadanie w bazie danych oraz wysyła nadzorcy polecenie do jego obliczenia.

\textbf{Baza danych} (\textbf{db\_master}) jest usługą zbudowaną w oparciu o bazę danych Redis. Jest to baza typu NoSQL działająca na zasadzie klucz-wartość. Wyboru dokonano ze względu na wydajność. Stworzone zostały trzy bazy do danego identyfikatora zadania: dla parametrów (na podstawie zapytania użytkownika), dla rezultatu (najlepsze znalezione rozwiązanie dla zadania) i dla statusu. Zadaniem usługi jest zarejestrowanie zadania, zwracanie statusu zadania, parametrów algorytmu oraz zapisanie najlepszego rozwiązania znalezionego przez nadzorcę zadania.

\textbf{Nadzorca} (\textbf{taskmaster}) jest usługą komunikującą są niemalże ze wszystkimi elementami systemu. Zgodnie z rysunkiem \ref{rys:parallel_algorithm_steps} jest bowiem odpowiedzialny za zebranie populacji utworzonych przez dany operator genetyczny, bądź generator populacji, a jednocześnie za podział jej na części, które będą wysyłane do replik usług tych operatorów. Dodatkowo przy scalaniu odpowiedzi z operatora selekcji, ustala najlepszy wynik, który jest trzymany między kolejnymi iteracjami algorytmu, w celu zwrócenia go po ostatnim powtórzeniu. Nadzorca rozpoczyna swoją pracę po otrzymaniu sygnału od dyspozytora zadań i tak jak pokazane jest to na rysunku \ref{rys:components_diagram}, odczytuje z bazy danych parametry algorytmu, a po zakończeniu działania, zapisuje do niego najlepszy znaleziony wynik.

Usługi operatorów genetycznych takie jak \textbf{krzyżowanie} (\textbf{cross\_operator}) i \textbf{mutacja} (\textbf{mutate\_operator}) przyjmują swoją część populacji, losują osobników podlegających mutacji, bądź krzyżowaniu, modyfikują populację i następnie zwracają ją do nadzorcy. \textbf{Selekcja} (\textbf{select\_operator}) natomiast oprócz przeprowadzenia selekcji, oblicza dla każdego osobnika funkcję celu, a następnie wybiera najlepszy rezultat, który również zwraca w odpowiedzi. Usługa \textbf{generacji populacji} (\textbf{population\_generator}) dostaje od nadzorcy specyfikację populacji, czyli bazowy chromosom oraz liczebność populacji docelowej.

\subsection{Diagram interakcji}
\label{subsec:diagram_interakcji}

Jeżeli chodzi o przepływ informacji w zaimplementowanym systemie, to można w tym miejscu wyróżnić przychodzące żądanie użytkownika z konkretnymi parametrami algorytmu (rysunek \ref{rys:sequence_user_request}), zapytanie o rezultat danego zadania (rysunek \ref{rys:sequence_user_result}) oraz obliczanie zadania zarządzane przez nadzorcę (rysunek \ref{rys:sequence_taskmaster}). Jako interfejs UI możemy rozumieć dowolne źródło, które wysyła żądania REST.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{images/sequence_user_request.pdf}
	\caption{Diagram interakcji dla żądania rozwiązania zadania przez użytkownika aplikacji Settler.}
	\label{rys:sequence_user_request}
\end{figure}

Zgodnie z rysunkiem \ref{rys:sequence_user_request} wysyłane jest żądanie HTTP POST z parametrami algorytmu, które wyłapane przez ui\_proxy jest tłumaczone na wiadomość gRPC i przesyłane do dyspozytora. Stamtąd całe zapytanie jest zapisywane w bazie pod unikalnym, wygenerowanym na podstawie danych identyfikatorem zwracanym do dyspozytora. Usługa wysyła tę wiadomość do nadzorcy (uruchamiając obliczenia algorytmu) oraz ui\_proxy, które to wysyła odpowiedź HTTP z kodem $200$ oraz identyfikatorem zadania w treści.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/sequence_user_result.pdf}
	\caption{Diagram interakcji dla rozwiązania zadania użytkownika aplikacji Settler.}
	\label{rys:sequence_user_result}
\end{figure}

Dzięki otrzymanemu identyfikatorowi, użytkownik (zgodnie z rysunkiem \ref{rys:sequence_user_result}) może wysłać zapytanie HTTP GET z żądaniem zasobu o tej cesze. Usługa ui\_proxy wysyła wiadomość gRPC do dyspozytora, który to pyta bazę danych o rozwiązanie zadania o danym identyfikatorze. W tym momencie istnieją przynajmniej dwa przypadki. W pierwszym (górna część rysunku \ref{rys:sequence_user_result}), jeżeli nadzorca wciąż prowadzi obliczenia dla danego zadania, w bazie istnieje status \textit{in progress} i to on jest zwracany kolejno do dyspozytora i ui\_proxy tak, że do użytkownika dociera informacja HTTP z kodem $202$. Jeżeli natomiast nadzorca zakończył prowadzenie obliczeń, a do bazy danych został zapisany rezultat, to zwracane jest rozwiązania dla najwyższej osiągniętej funkcji celu (\textit{best fit}), przetłumaczone przez ui\_proxy na wiadomość HTTP z kodem $200$.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{images/sequence_taskmaster.pdf}
	\caption{Diagram interakcji dla zadań usługi nadzorcy aplikacji Settler.}
	\label{rys:sequence_taskmaster}
\end{figure}

Najciekawsza część wymiany komunikacji jest jednak przy prowadzeniu obliczeń dla danego zadania. Rysunek \ref{rys:sequence_taskmaster} jest kontynuacją procesu rozpoczętego przez dyspozytora po otrzymaniu żądania od użytkownika na rysunku \ref{rys:sequence_user_request}. Nadzorca odpytuje wtedy bazę danych o konkretne parametry algorytmu dla zadania, a po ich otrzymaniu tworzy pierwszy chromosom, będący zakodowaną wersją rozwiązania. Dodatkowo zgodnie ze zdefiniowanym przez użytkownika współczynnikiem, wysyła zadaną ilość razy żądanie do usługi generowania populacji o wygenerowanie jej na podstawie chromosomu oraz liczebności. Po otrzymaniu odpowiedzi dla każdego zapytania, scala populację, a następnie w pętli, zadaną ilość razy wykonuje kroki algorytmu genetycznego. Kolejno wysyła dane dotyczące relacji, miejsc, priorytetu osób, populacji (podzielonej między kilka zapytań), najważniejszego punktu mapy, współczynnika algorytmu oraz priorytetów osób do usługi dokonującej selekcji. Po otrzymaniu i zabraniu odpowiedzi w jedną populację oraz wyznaczeniu najlepszego wyniku, wysyła ją rozdzieloną wraz ze współczynnikiem mutacji do operatora mutacji. Znów po otrzymaniu, scaleniu i ponownym podzieleniu wysyła populację z odpowiednim współczynnikiem do operatora krzyżowania. Po wykonaniu zadanej ilości iteracji, najlepsze znalezione rozwiązanie wraz z identyfikatorem zadania jest odsyłane do bazy danych do zapisania.

\subsection{Diagram przypadków użycia}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{images/use_case_diagram.pdf}
	\caption{Diagram przypadków użycia dla najważniejszych aktorów aplikacji Settler.}
	\label{rys:use_case_diagram}
\end{figure}

Dokonując przeglądu możliwych działań w systemie, należy zaznaczyć, że najważniejszymi aktorami z perspektywy aplikacji Settler są: użytkownik wysyłający żądania obliczenia algorytmu oraz aktor nieosobowy - usługa nadzorcy. Pierwszy z nich ma możliwość zapytania o najlepsze ułożenie dla zadanej mapy, listy osób, ich relacji i priorytetów oraz najważniejszego miejsca (rysunek \ref{rys:sequence_user_request}). Przy tym podać musi dodatkowe parametry algorytmu genetycznego. Dzięki temu zostaje stworzone zadanie o wygenerowanym, unikalnym identyfikatorze. To znów powoduje rozpoczęcie obliczeń przez nadzorcę (rysunek \ref{rys:sequence_taskmaster}). Po zakończeniu wszystkich iteracji przez nadzorcę i zapisaniu najlepszego wyniku do bazy danych, sprawdzenie rezultatu zadania zwróci inny wynik, niż miało to miejsce w trakcie przeprowadzania obliczeń (rysunek \ref{rys:sequence_user_result}).

\subsection{Komunikacja między mikrousługami}
\label{subsec:komunikacja_aplikacja}

W podsekcji \ref{subsec:diagram_komponentow} wspomniano, że mikrousługi komunikują się ze sobą wewnętrznie za pomocą gRPC opisanego w rozdziale \ref{subsec:grpc}. Za pomocą pliku konfiguracyjnego Protocol Buffers stworzono interfejs komunikacyjny umożliwiający przeprowadzanie wszystkich akcji opisanych w rozdziale poświęconym interakcjom \ref{subsec:diagram_interakcji}. W kodzie \ref{lst:settler_proto_services} zobaczyć można, że połączenia z rysunku \ref{rys:components_diagram} bezpośrednio można spiąć z definicjami metod kolejnych usług Protocol Buffers. Dzięki temu od razu można stwierdzić, które funkcjonalności zostały pokryte, a które jeszcze powinny zostać zaimplementowane.

\begin{listing}[h]
\caption{Definicja części usług aplikacji Settler.}
\label{lst:settler_proto_services}
\begin{lstlisting}[language=protobuf2,frame=lines,numbers=left]
syntax = "proto3";
package settler;

service Dispatcher {
    rpc HandleJobRequest(AlgorithmParams) returns (JobId) {}
    rpc GetJobResult(JobId) returns (AlgorithmResult) {}
}

service Taskmaster {
    rpc DoJobId(JobId) returns (Empty) {}
}

service PopulationGenerator {
    rpc ProducePopulation(PopulationRequest) returns (Population) {}
}

service SelectOperator {
    rpc ProduceSelectedPopulation(SelectionRequest) returns (Population) {}
}

service MutateOperator {
    rpc ProduceMutatedPopulation(GeneticOperatorRequest) returns (Population) {}
}
\end{lstlisting}
%\small\textsuperscript{Źródło: \cite{protocol_buffers_doc}}
\end{listing}

Jeżeli natomiast chodzi o komunikaty, to zostało zdefiniowanych wiele złożonych wiadomości ułatwiających pokrycie wszystkich przypadków użycia ich w usługach. Przykładem może tu być komunikat zawierający informację o rezultacie algorytmu z kodu \ref{lst:settler_proto_message}. Zgodnie z kodem \ref{lst:settler_proto_services}, wiadomość ta musi być zawsze odpowiedzią na żądanie o rezultat wysłany do dyspozytora. Dlatego też należało tu obsłużyć nie tylko samo rozwiązanie (\textit{result} oraz \textit{result\_fit}), ale również status zadania w przypadku, gdy rezultatu jeszcze by nie było.

\begin{listing}[h]
\caption{Definicja wiadomości przechowującej informację o rezultacie rozwiązania algorytmu dla danego zadania aplikacji Settler.}
\label{lst:settler_proto_message}
\begin{lstlisting}[language=protobuf2,frame=lines,numbers=left]
message AlgorithmResult {
    enum AlgorithmStatus {
        READY = 0;
        IN_PROGRESS = 1;
        CREATED = 2;
        NON_EXISTANT = 3;
    }

    AlgorithmStatus status = 1;
    repeated SettledPerson result = 2;
    float result_fit = 3;
}
\end{lstlisting}
%\small\textsuperscript{Źródło: \cite{protocol_buffers_doc}}
\end{listing}

Definiowanie interfejsów komunikacyjnych za pomocą Protocol Buffers znacznie ułatwiło weryfikację spójności wymienianych między usługami informacji. Ten protokół jednak nie jest czytelny dla przeglądarek internetowych używających zazwyczaj zapytań HTTP REST. W trakcie implementacji sytemu Settler sprawdzono dwa rozwiązania, które pomagają w radzeniu sobie z tym trybem komunikacji.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{images/swagger.png}
	\caption{Część interaktywnej strony wygenerowanej za pomocą narzędzia Swagger dla opisu API aplikacji Settler.}
	\label{rys:swagger_settler}
\end{figure}

\begin{listing}[H]
\caption{Przykładowa definicja usługi aplikacji Settler przy użyciu grpc-gateway.}
\label{lst:settler_proto_grpc_gateway}
\begin{lstlisting}[language=protobuf2,frame=lines,numbers=left]
syntax = "proto3";
package settler;

import "google/api/annotations.proto";

service Dispatcher {
    rpc HandleJobRequest(AlgorithmParams) returns (JobId) {
        option (google.api.http) = {
            post: "/v1/job",
            body: "*"
        };
    }
    rpc GetJobResult(JobId) returns (AlgorithmResult) {
        option (google.api.http) = {
            get: "/v1/job/{id}"
        };
    }
}
\end{lstlisting}
%\small\textsuperscript{Źródło: \cite{protocol_buffers_doc}}
\end{listing}

Na początku warto zaznaczyć, że każde REST API warto w czytelny dla użytkownika sposób udokumentować. Jednym, z narzędzi używanych do wygenerowania interaktywnej strony z opisem API na podstawie pliku konfiguracyjnego JSON jest Swagger \cite{swagger_io}. Dla systemu Settler również powstała taka strona, której nagłówek i część opisu znajduje się na rysunku \ref{rys:swagger_settler}. Plik konfiguracyjny natomiast został stworzony ręcznie. Poniżej wyjaśniono, dlaczego.

Społeczność pracująca nad funkcjonalnościami gRPC zaimplementowała rozwiązanie o nazwie \textit{grpc-gateway} \cite{grpc_gateway}, umożliwiające automatyczne wygenerowanie proxy, będącego w stanie obsłużyć zapytania REST poprzez jego przetłumaczenie na komunikat gRPC i przesłanie do usługi. Podobnym narzędziem jest Envoy \cite{envoy_io}, który obsługuje również query string przy zapytaniach agregujących. Dodatkowo z pomocą tego narzędzia automatycznie generowany jest plik konfiguracyjny Swagger w celu zapewnienia interaktywnego opisu API aplikacji. Jak widoczne jest w kodzie \ref{lst:settler_proto_grpc_gateway}, do definicji metod usługi wystarczy dodać informację na temat zapytania HTTP z dokładnym adresem. Okazuje się jednak, że narzędzie ma wsparcie jedynie dla języka Go i niemożliwe jest stworzenie obiektów Protocol Buffers możliwych do przeczytania w usługach implementowanych w języku Python. Ze względu na te ograniczenia zrezygnowano z użycia tej biblioteki i zdecydowano, że konfiguracja narzędzia Swagger powstanie poza definicją Protocol Buffers. Można mieć nadzieję, że w przyszłości nie trzeba będzie wprowadzać do projektu takiej redundancji.

\subsection{Reprezentacja aplikacji w platformie Kubernetes (schemat obiektów)}

\begin{listing}[H]
\caption{Definicja obiektu Service dla usługi db\_master aplikacji Settler.}
\label{lst:settler_service}
\begin{lstlisting}[language=yaml,frame=lines,numbers=left]
apiVersion: v1
kind: Service
metadata:
  name: db-master
spec:
  selector:
    app: db-master
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
\end{lstlisting}
%\small\textsuperscript{Źródło: \cite{kubernetes_io}}
\end{listing}

\begin{listing}[h]
\caption{Definicja obiektu Deployment dla usługi db\_master aplikacji Settler.}
\label{lst:settler_deployment}
\begin{lstlisting}[language=yaml,frame=lines,numbers=left]
apiVersion: apps/v1
kind: Deployment
metadata:
  name: db-master
  labels:
    app: db-master
spec:
  selector:
    matchLabels:
      app: db-master
  replicas: 1
  template:
    metadata:
      labels:
        app: db-master
    spec:
      containers:
      - name: db-master
        image: littlem7/db-master:latest
        ports:
        - containerPort: 8080
        env:
        - name: REDIS_DB_HOSTNAME
          value: "redis-db"
        - name: REDIS_DB_PORT
          value: "6379"

\end{lstlisting}
%\small\textsuperscript{Źródło: \cite{kubernetes_io}}
\end{listing}

Część podsekcji \ref{subsec:porownanie} poświęcona platformie Kubernetes omawia podstawowe obiekty tego rozwiązania. W celu wdrożenia omówionej aplikacji Settler zdefiniowano obiekty Service oraz Deployment dla każdej z usług zdefiniowanych na rysunku \ref{rys:components_diagram}. Przykładowo dla mikrousługi zajmującej się obsługą bazy danych, kod obiektu Deployment \ref{lst:settler_deployment}, poza elementami omówionymi na przykładzie kodu \ref{lst:deployment}, zawiera definicję zmiennych środowiskowych, do których będzie miał dostęp obiekt Pod z kontenerem aplikacyjnym. Dzięki temu możliwe jest znalezienie adresu innej mikrousługi w systemie. W obiekcie Service (przykładowy kod \ref{lst:settler_service}) natomiast nie było potrzeby definiowania dodatkowych pól, ale konieczne było zdefiniowanie samego obiektu w celu upublicznienia usług.

\subsection{Testy aplikacji}

W celu zapewnienia ciągłej integracji systemu Settler, za pomocą pluginu narzędzia Gitlab sporządzono pipeline, który po każdej zmianie kodu w repozytorium budował zmienianą usługę, testował ją, dokonywał statycznej analizy kodu za pomocą narzędzia \textit{pylint}, a następnie, po udanej weryfikacji budował obraz Docker danej usługi i publikował go w bibliotece Docker Hub. Średnie pokrycie testami aplikacji wynosi obecnie $82\%$. Zrzut ekranu \ref{rys:gitlab_ci} został wykonany dla testów przy zmianie w interfejsie - pliku konfiguracyjnego Protocol Buffers. Dzięki odpowiedniej konfiguracji zostały przetestowane i przebudowane wszystkie mikrousługi.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{images/gitlab_ci.png}
	\caption{Przykładowy pipeline stworzony za pomocą narzędzia gitlab-ci dla aplikacji Settler przy zmianie mającej wpływ na wszystkie mikrousługi.}
	\label{rys:gitlab_ci}
\end{figure}

\section{Wdrożenie aplikacji}

\subsection{Wdrożenie w lokalnym klastrze}
\label{subsec:wdrozenie_lokalne}

Jak wspomniano w części poświęconej narzędziom do budowania klastra Kubernetes w podsekcji \ref{subsec:rozliczenie}, obecnie najpowszechniej używanym narzędziem jest k3d. Jest niezwykle łatwy w obsłudze, co pokazuje zrzut ekranu \ref{rys:k3d_cluster}, na którym stworzenie całego klastra sprowadza ten proces do wywołania jednej komendy. Wywołanie \textit{k3d create} tworzy klaster Kubernetes pod domyślną nazwą \textit{k3s-deafult} w kontenerze Docker \textit{k3d-k3s-default-server}. Aby móc działać w obrębie klastra (na przykład przy użyciu narzędzia kubectl), należy wskazać odpowiedni plik \textit{kubeconfig}, który zawiera konfigurację umożliwiającą dostęp do klastra (certyfikat dostępu oraz użytkowników). Taki plik jest również automatycznie generowany przez k3d i można go wskazać przez ustawienie zmiennej \textit{\mbox{KUBECONFIG}}, bądź wpisanie komendy \textit{k3d shell}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{images/k3d_cluster.png}
	\caption{Stworzenie klastra Kubernetes z użyciem narzędzia k3d.}
	\label{rys:k3d_cluster}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{images/k3d_manifests_run.png}
	\caption{Stworzenie usług aplikacji Settler w klastrze Kubernetes za pomocą narzędzia kubectl.}
	\label{rys:k3d_manifests_run}
\end{figure}

Po stworzeniu klastra można przystąpić do wdrożenia aplikacji. Przy pomocy komendy \textit{kubectl apply} manifesty definiujące obiekty platformy Kubernetes dla aplikacji Settler są czytane i odpowiednio tworzone w klastrze tak, jak pokazuje to zrzut ekranu \ref{rys:k3d_manifests_run}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{images/settler_replicasets.png}
	\caption{Obiekty typu ReplicaSet dla usług aplikacji Settler w klastrze Kubernetes.}
	\label{rys:settler_replicasets}
\end{figure}

Zgodnie z tym, co zostało omówione w podsekcji \ref{subsec:porownanie}, pomimo, że jednoznacznie obiekty typu Pod nie zostały zdefiniowane w manifestach, to występują w konfiguracji Deployment i dlatego zostały stworzone. Za pomocą narzędzia kubectl można na bieżąco sprawdzać status oraz logi obiektów. Przy próbie wypisania wszystkich obiektów Pod w klastrze dla obecnego zakresu nazw za pomocą komendy \textit{kubectl get pods}, administrator otrzymuję pełną listę utworzonych obiektów tego typu. Warto zauważyć, że dla usługi operatorów genetycznych (\textit{service/cross-operator}, \textit{service/mutate-operator}, \textit{service/select-operator} oraz \textit{service/population-generator}) pojawiają się po trzy obiekty typu Pod zgodnie z konfiguracją (zrzut ekranu \ref{rys:k3d_manifests_run}). Zrzut ekranu \ref{rys:settler_replicasets} pokazuje wszystkie obiekty typu ReplicaSet. Widoczna jest tutaj informacja ile obiektów Pod ma być docelowo dla danej usługi oraz ile z nich zostało już przygotowanych.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{images/working_example.png}
	\caption{Działanie aplikacji Settler dla żądania POST z poleceniem obliczenia algorytmu.}
	\label{rys:working_example}
\end{figure}

By sprawdzić działanie aplikacji, zgodnie ze zrzutem ekranu \ref{rys:working_example}, można użyć komendy \textit{kubectl port-forward} dla usługi ui\_proxy w celu przekazania ruchu pochodzącego z komendy curl bezpośrednio do tej usługi. Otrzymujemy dzięki temu odpowiedź $202$ (zgodnie z dokumentacją w podsekcji \ref{subsec:diagram_interakcji}) wraz z identyfikatorem zadania, o którego status można dalej pytać.

\subsection{Wdrożenie w chmurze publicznej}

Jednym z najbardziej popularnych dostarczycieli chmury publicznej jest Google Cloud. Ze względu na łatwość użycia i darmowy dostęp do części funkcjonalności, zdecydowano się użyć właśnie tej platformy przy wdrożeniu aplikacji Settler. Istnieje możliwość stworzenia kilku typów klastrów w celu zapewnienia dostępu do aplikacji z różnych części świata \cite{google_kubernetes_engine_doc}. By jednak zacząć pracę z Google Cloud Engine - usługi Google Cloud opartej na platformie Kubernetes należy:

\begin{itemize}
    \item aktywować funkcję Google Kubernetes Engine API na swoim koncie,
    \item zainstalować lokalnie narzędzie (CLI) do obsługi Google Cloud, czyli Cloud SDK (gcloud).
\end{itemize}

Po wykonaniu tych kroków można przystąpić do stworznia klastra. Dokonać można tego poprzez graficzny interfejs platformy Google Cloud, albo za pomocą narzędzia gcloud. Szczegółowa instrukcja znajduje się w dokumentacji Google Kubernetes Engine \cite{google_kubernetes_engine_doc}, natomiast najważniejszymi konfiguracjami są: ilość węzłów w puli, rodzaj maszyn w klastrze oraz region, bądź strefa, w której klaster ma działać.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{images/klaster_glcloud.png}
	\caption{Stworzony klaster Kubernetes w Google Kubernetes Engine.}
	\label{rys:klaster_glcloud}
\end{figure}

Po stworzeniu klastra, w graficznym interfejsie użytkownika widoczny jest opis obiektu wraz z jego zasobami, jak to pokazuje zrzut ekranu \ref{rys:klaster_glcloud}. Po naciśnięciu przycisku \textit{Połącz} po prawej stronie, wyświetla się komenda służąca do ściągnięcia (za pomocą komendy gcloud) danych wrażliwych służących do dostępu do klastra (kubeconfig wspomniany we wcześniejszym rozdziale \ref{subsec:wdrozenie_lokalne}). Dzięki wykonaniu tej komendy można zarządzać obiektami platformy Kubernetes za pomocą narzędzia kubectl, jak zostało to również zaprezentowane w podsekcji \ref{subsec:wdrozenie_lokalne}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{images/settler_example_pods.png}
	\caption{Część obiektów Pod aplikacji Settler na wybranym węźle dla domyślnego Namespace w Google Kubernetes Engine.}
	\label{rys:settler_example_pods}
\end{figure}

Po uruchomieniu wszystkich obiektów na postawie plików konfiguracyjnych za pomocą komendy \textit{kubectl apply}, jak można zauważyć na zrzucie ekranu \ref{rys:settler_example_pods}, na węzłach w puli klastra pojawiają się obiekty Pod związane z wdrażaną aplikacją. W przypadku, gdy występują jakiekolwiek problemy z postawieniem obiektów (przykładowo za mała ilość CPU z węzłów puli), można je sprawdzić z użyciem komendy \textit{kubectl describe pod <pod\_name>}, bądź graficznego interfejsu użytkownika Google Cloud.


%\subsection{Zarządzanie wdrożonymi aplikacjami}


%\section{Monitorowanie mikrousług}

% rozwiązania pomocnicze

%\subsection{Opis działania}
%\subsection{Przegląd dostępnych rozwiązań}
